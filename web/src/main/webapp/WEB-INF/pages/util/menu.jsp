<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<ul class="nav nav-tabs nav-fill">
    <sec:authorize access="hasAuthority('VIEW_USERS')">
        <li class="nav-item">
            <a class="nav-link <c:if test='${requestScope.active=="users"}'>active</c:if>"
               href="${pageContext.request.contextPath}/users">Users</a>
        </li>
    </sec:authorize>
    <sec:authorize access="hasAnyAuthority('VIEW_ITEMS', 'VIEW_ORDERS', 'VIEW_NEWS')">
        <li class="nav-item">
            <a class="nav-link <c:if test='${requestScope.active=="items"}'>active</c:if>"
               href="${pageContext.request.contextPath}/items">Items</a>
        </li>
        <li class="nav-item">
            <sec:authorize access="hasAuthority('CREATE_ITEMS')">
                <a class="nav-link <c:if test='${requestScope.active=="orders"}'>active</c:if>"
                   href="${pageContext.request.contextPath}/orders">Orders</a>
            </sec:authorize>
            <sec:authorize access="hasAuthority('CREATE_ORDERS')">
                <a class="nav-link <c:if test='${requestScope.active=="orders"}'>active</c:if>"
                   href="${pageContext.request.contextPath}/orders/user">Orders</a>
            </sec:authorize>
        </li>
        <li class="nav-item">
            <a class="nav-link <c:if test='${requestScope.active=="news"}'>active</c:if>"
               href="${pageContext.request.contextPath}/news">News</a>
        </li>
    </sec:authorize>
    <li class="nav-item dropdown">
        <jsp:include page="account.jsp"/>
    </li>
</ul>