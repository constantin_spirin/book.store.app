<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div>
    <c:if test="${pagesCount > 1}">
        <nav>
            <ul class="pagination">
                <c:if test="${page != 0}">
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/${requestScope.entity}?page=${page-1}">Previous</a>
                    </li>
                </c:if>
                <c:forEach begin="0" end="${pagesCount - 1}" step="1" var="i">
                    <c:choose>
                        <c:when test="${i == page}">
                            <li class="page-item active">
                                <a class="page-link" href="#">${i+1}</a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="page-item">
                                <a class="page-link"
                                   href="${pageContext.request.contextPath}/${requestScope.entity}?page=${i}">${i + 1}</a>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:if test="${page != (pagesCount - 1)}">
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/${requestScope.entity}?page=${page+1}">Next</a>
                    </li>
                </c:if>
            </ul>
        </nav>
    </c:if>
</div>