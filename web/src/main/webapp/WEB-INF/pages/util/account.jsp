<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
   aria-expanded="false">Account</a>
<div class="dropdown-menu">
    <a class="dropdown-item">
        <sec:authentication property="name"/>
    </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#">Edit profile</a>
    <a class="dropdown-item btn btn-success" href="${pageContext.request.contextPath}/login?logout">Logout</a>
</div>