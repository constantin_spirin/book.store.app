<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Register user page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>
    <form:form action="${pageContext.request.contextPath}/create" modelAttribute="user" method="post">
        <form:errors path="*" cssClass="errorblock" element="div"/>
        <div class="form-group">
            <form:label path="name">First name</form:label>
            <form:input path="name" class="form-control" placeholder="First name"/>
        </div>
        <div class="form-group">
            <form:label path="surname">Last name</form:label>
            <form:input path="surname" class="form-control" placeholder="Last name"/>
        </div>
        <div class="form-group">
            <form:label path="email">Email address</form:label>
            <form:input path="email" type="email" clss="form-comtrol" placeholder="Email"/>
        </div>
        <div class="form-group">
            <form:label path="password">Password</form:label>
            <form:input path="password" type="pasword" class="form-control" placeholder="Password"/>
        </div>
        <div class="form-group">
            <form:label path="address">Address</form:label>
            <form:input path="address" type="address" class="form-control" placeholder="Address"/>
        </div>
        <div class="form-group">
            <form:label path="telephone">Telephone</form:label>
            <form:input path="telephone" type="telephone" class="form-control" placeholder="Telephone"/>
        </div>
        <button type="submit" class="btn btn-primary">Register</button>
    </form:form>
</div>
<jsp:include page="util/js.jsp"/>
</body>
</html>
