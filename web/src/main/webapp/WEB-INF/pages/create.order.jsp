<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="div" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Create Order page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <c:set var="active" value="orders" scope="request"></c:set>
    <jsp:include page="util/menu.jsp"/>

    <div class="col-top-bottom-padding-15px">
        <h3>Place an order</h3>
    </div>

    <form:form action="${pageContext.request.contextPath}/orders/create" modelAttribute="order"
               method="post">
        <form:errors path="*" cssClass="errorblock" element="div"/>
        <form:hidden path="itemId"/>
        <div class="form-group">
            <form:label path="itemTitle">Title: ${order.itemTitle}</form:label>
        </div>
        <div class="form-group">
            <form:label path="price">Price: ${order.price}</form:label>
        </div>
        <div class="form-group">
            <form:label path="price">Description: ${order.description}</form:label>
        </div>
        <div class="form-group row">
            <div class="col-3">
                <form:label path="quantity">Quantity: </form:label>
                <input type="number" class="form-control" name="quantity" id="quntity" min="1" step="1">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Order</button>
    </form:form>

</div>


<jsp:include page="util/js.jsp"/>
</body>
</html>