<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Creating item page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

        <c:set var="active" value="items" scope="request"></c:set>
        <jsp:include page="util/menu.jsp"/>

    <div class="col-top-bottom-padding-15px">
        <h3>Create item</h3>
    </div>

    <form:form action="${pageContext.request.contextPath}/items/create" modelAttribute="item"
               method="post">
        <form:errors path="*" cssClass="errorblock" element="div"/>
    <div class="form-group">
        <form:label path="name">Title</form:label>
        <form:input path="name" class="form-control" placeholder="Title"/>
    </div>
    <div class="form-group">
        <form:label path="uniqueNumber">Unique number</form:label>
        <form:input path="uniqueNumber" class="form-control" placeholder="Unique number"/>
    </div>
    <div class="form-group">
        <form:label path="description">Description</form:label>
        <form:textarea path="description" class="form-control" placeholder="Description"/>
    </div>
    <div class="form-group">
        <form:label path="price">Price</form:label>
        <form:input path="price" class="form-control" placeholder="Price"/>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
    </form:form>

    <jsp:include page="util/js.jsp"/>
</body>
</html>