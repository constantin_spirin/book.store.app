<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>News page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <c:set var="active" value="news" scope="request"></c:set>
    <jsp:include page="util/menu.jsp"/>

    <sec:authorize access="hasAuthority('CREATE_NEWS')">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="${pageContext.request.contextPath}/news/create">CREATE</a>
                </div>
            </div>
        </nav>
    </sec:authorize>

    <sec:authorize access="hasAuthority('DELETE_NEWS')">
    <form action="${pageContext.request.contextPath}/news/delete" method="post">
        </sec:authorize>

        <table class="table">
            <thead>
            <tr>
                <sec:authorize access="hasAnyAuthority('DELETE_NEWS')">
                    <th scope="col">#</th>
                </sec:authorize>
                <th scope="col">CREATED</th>
                <th scope="col">TITLE</th>
                <th scope="col">AUTHOR</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${news}" var="aNews">
                <tr>
                    <sec:authorize access="hasAnyAuthority('DELETE_NEWS')">
                        <td>
                            <input type="checkbox" name="ids" value="${aNews.id}"/>
                        </td>
                    </sec:authorize>
                    <td>${aNews.created}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/news/${aNews.id}">${aNews.title}</a>
                    </td>
                    <td>${aNews.author}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <sec:authorize access="hasAuthority('DELETE_NEWS')">
        <div class="col-top-bottom-padding-15px">
            <input type="submit" class="btn btn-primary" aria-pressed="true" value="DELETE">
        </div>
    </form>
    </sec:authorize>

    <c:set var="entity" value="news" scope="request"/>
    <jsp:include page="util/pagination.jsp"/>

</div>

<jsp:include page="util/js.jsp"/>
</body>
</html>