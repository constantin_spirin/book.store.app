<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Login page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 shadow-lg bg-white rounded">
            <form:form action="${pageContext.request.contextPath}/login" method="post" modelAttribute="user">
                <form:errors path="*" cssClass="errorblock" element="div"/>
                <div class="form-group">
                    <form:label path="email">Email address</form:label>
                    <form:input path="email" type="email" class="form-comtrol" placeholder="Email"/>
                </div>
                <div class="form-group">
                    <form:label path="password">Password</form:label>
                    <form:password path="password" class="form-control"/>
                </div>
                <button type="submit" class="btn btn-success">Log in</button>
            </form:form>
            <div class="row">
                <a href="${pageContext.request.contextPath}/register">Register</a>
            </div>
        </div>
        <div class="col-md-4"> </div>
    </div>
</div>

<jsp:include page="util/js.jsp"/>
</body>
</html>