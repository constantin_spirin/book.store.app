<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Users page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <c:set var="active" value="users" scope="request"></c:set>
    <jsp:include page="util/menu.jsp"/>

    <form id="disableForm" action="${pageContext.request.contextPath}/users/disable " method="post"
          class="actions inline-block"
          onsubmit="$('[name = ids]').attr('form', 'disableForm');">
        <input type="submit" value="DISABLE" class="btn btn-primary" aria-pressed="true">
    </form>
    <form id="enableForm" action="${pageContext.request.contextPath}/users/enable " method="post"
          class="actions inline-block"
          onsubmit="$('[name = ids]').attr('form', 'enableForm');">
        <input type="submit" value="ENABLE" class="btn btn-primary" aria-pressed="true">
    </form>
    <form id="deleteForm" action="${pageContext.request.contextPath}/users/delete " method="post"
          class="actions inline-block"
          onsubmit="$('[name = ids]').attr('form', 'deleteForm');">
        <input type="submit" value="DELETE" class="btn btn-primary" aria-pressed="true">
    </form>
    <form id="restoreForm" action="${pageContext.request.contextPath}/users/restore " method="post"
          class="actions inline-block"
          onsubmit="$('[name = ids]').attr('form', 'restoreForm');">
        <input type="submit" value="RESTORE" class="btn btn-primary" aria-pressed="true">
    </form>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
            <th scope="col">Full Name</th>
            <th scope="col">Enabled</th>
            <th scope="col">Deleted</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${users}" var="user">
            <tr>
                <th scope="row">
                    <input form="" type="checkbox" name="ids" value="${user.id}">
                </th>
                <td>${user.email}</td>
                <td>${user.role}</td>
                <td>${user.fullName}</td>
                <td>${user.getEnable()}</td>
                <td>${user.getDeleted()}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/users/${user.id}"
                       class="btn btn-primary" aria-pressed="true"
                       role="button">UPDATE</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <c:set var="entity" value="users" scope="request"/>
    <jsp:include page="util/pagination.jsp"/>

</div>


<jsp:include page="util/js.jsp"/>
</body>
</html>