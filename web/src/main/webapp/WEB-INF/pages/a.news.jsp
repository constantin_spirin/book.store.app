<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>${news.title}</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <c:set var="active" value="news" scope="request"></c:set>
    <jsp:include page="util/menu.jsp"/>

    <sec:authorize access="hasAuthority('UPDATE_NEWS')">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link"
                       href="${pageContext.request.contextPath}/news/update/${news.id}">EDIT</a>
                </div>
            </div>
        </nav>
    </sec:authorize>

    <h3>${news.title}</h3>
    <p>${news.content}</p>


    <sec:authorize access="hasAuthority('CREATE_COMMENTS')">


        <form:form action="${pageContext.request.contextPath}/news/${news.id}/comments/create" modelAttribute="comment"
                   method="post">
            <form:errors path="*" cssClass="errorblock" element="div"/>
            <div class="form-group">
                <form:label path="content">Add comment</form:label>
                <form:textarea path="content" class="form-control" placeholder="Your comment"/>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form:form>
    </sec:authorize>

    <div class="row">
        <div class="col-md-12 col-top-bottom-padding-15px">
            <h5>
                Comments
            </h5>
        </div>
    </div>

    <sec:authorize access="hasAuthority('DELETE_COMMENTS')">
    <form action="${pageContext.request.contextPath}/news/${news.id}/comments/delete" method="post">
        </sec:authorize>
        <c:forEach var="comment" items="${comments}">
            <sec:authorize access="hasAuthority('DELETE_COMMENTS')">
                <div class="form-check">
                    <input class="form-check-input position-static" type="checkbox" name="ids" value="${comment.id}">
                </div>
            </sec:authorize>
            <div class="border-bottom bg-light">
                <div class="row">
                    <div class="col-md-8 font-weight-bold">${comment.author}</div>
                    <div class="col-md-4 text-right">${comment.created}</div>
                </div>
                <div class="row">
                    <div class="col-md-12">${comment.content}</div>
                </div>
            </div>
        </c:forEach>


        <sec:authorize access="hasAuthority('DELETE_COMMENTS')">
        <div class="col-top-bottom-padding-15px">
            <input type="submit" class="btn btn-primary" aria-pressed="true" value="DELETE">
        </div>
    </form>
    </sec:authorize>


    <div class="col-md-12 col-top-bottom-padding-15px">
        <c:set var="entity" value="news/${news.id}" scope="request"/>
        <jsp:include page="util/pagination.jsp"/>
    </div>
</div>


<jsp:include page="util/js.jsp"/>
</body>
</html>