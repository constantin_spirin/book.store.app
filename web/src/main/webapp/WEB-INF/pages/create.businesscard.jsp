<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Creating business card page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <form:form action="${pageContext.request.contextPath}/businesscards/create" modelAttribute="businessCard"
               method="post">
        <form:errors path="*" cssClass="errorblock" element="div"/>
    <div class="form-group">
        <form:label path="title">Title</form:label>
        <form:input path="title" class="form-control" placeholder="Title"/>
    </div>
    <div class="form-group">
        <form:label path="fullName">Full Name</form:label>
        <form:input path="fullName" class="form-control" placeholder="Full Name"/>
    </div>
    <div class="form-group">
        <form:label path="workingTelephone">Working Telephone</form:label>
        <form:input path="workingTelephone" class="form-comtrol" placeholder="Telephone"/>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
    </form:form>


    <jsp:include page="util/js.jsp"/>
</body>
</html>