<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Update(for Admin) user page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <c:set var="active" value="users" scope="request"></c:set>
    <jsp:include page="util/menu.jsp"/>

    <div class="form-group">
        First name: ${user.name}
    </div>
    <div class="form-group">
        Last name: ${user.surname}
    </div>
    <div class="form-group">
        Email address: ${user.email}
    </div>
    <form:form action="${pageContext.request.contextPath}/users/updatepassword" modelAttribute="user" method="post">
        <form:errors path="*" cssClass="errorblock" element="div"/>
        <div class="form-group">
            <form:label path="password">New Password</form:label>
            <form:input path="password" type="pasword" class="form-control" placeholder="Password"/>
        </div>
        <form:hidden path="id"/>
        <button type="submit" class="btn btn-primary">Save</button>
    </form:form>
    <form:form action="${pageContext.request.contextPath}/users/updaterole" modelAttribute="user" method="post">
        <form:errors path="*" cssClass="errorblock" element="div"/>
        <div class="form-group">
            <form:label path="roleId">Change Role</form:label>
            <form:select path="roleId">
                <form:options items="${roles}" itemValue="id" itemLabel="name"/>
            </form:select>
        </div>
        <form:hidden path="id"/>
        <button type="submit" class="btn btn-primary">Save</button>
    </form:form>
</div>
<jsp:include page="util/js.jsp"/>
</body>
</html>
