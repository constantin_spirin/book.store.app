<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Create user page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <form:form action="${pageContext.request.contextPath}/users/create" modelAttribute="user" method="post">
        <form:errors path="*" cssClass="errorblock" element="div"/>
        <div class="form-group">
            <form:label path="name">First name</form:label>
            <form:input path="name" class="form-control" placeholder="First name"/>
        </div>
        <div class="form-group">
            <form:label path="surname">Last name</form:label>
            <form:input path="surname" class="form-control" placeholder="Last name"/>
        </div>
        <div class="form-group">
            <form:label path="email">Email address</form:label>
            <form:input path="email" type="email" clss="form-comtrol" placeholder="Email"/>
        </div>
        <div class="form-group">
            <form:label path="password">Password</form:label>
            <form:input path="password" type="pasword" class="form-control" placeholder="Password"/>
        </div>
        <div class="form-group">
            <form:label path="role">Role</form:label>
            <form:select path="role">
                <form:options items="${roles}" itemValue="id" itemLabel="name"/>
            </form:select>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form:form>
</div>
<jsp:include page="util/js.jsp"/>
</body>
</html>
