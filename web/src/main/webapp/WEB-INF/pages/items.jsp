<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Items page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <c:set var="active" value="items" scope="request"></c:set>
    <jsp:include page="util/menu.jsp"/>

    <sec:authorize access="hasAuthority('CREATE_ITEMS')">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="${pageContext.request.contextPath}/items/create">CREATE</a>
                    <a class="nav-item nav-link" data-toggle="collapse" data-target="#collapseInputFile"
                       aria-expanded="false" aria-controls="collapseInputFile" href="">UPLOAD</a>
                </div>
            </div>
        </nav>
        <div class="collapse" id="collapseInputFile">
            <div class="card card-body">
                <form:form method="POST" action="${pageContext.request.contextPath}/items/upload" modelAttribute="file"
                           enctype="multipart/form-data">

                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="multipartFile" name="multipartFile"
                                   onchange='$("#upload-file-info").html($(this).val());'>
                            <label id="upload-file-info" class="custom-file-label" for="multipartFile"
                                   aria-describedby="inputGroupFileAddon02">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <input type="submit" class="input-group-text" id="inputGroupFileAddon02" value="Upload"/>
                        </div>
                    </div>

                    <div class="row text-danger">
                            ${xmlValidationError}
                    </div>
                </form:form>
            </div>
        </div>
    </sec:authorize>

    <sec:authorize access="hasAuthority('CREATE_ITEMS')">
    <form action="${pageContext.request.contextPath}/items/delete" method="post">
        </sec:authorize>

        <table class="table">
            <thead>
            <tr>
                <sec:authorize access="hasAuthority('UPDATE_ITEMS')">
                    <th scope="col">#</th>
                </sec:authorize>
                <th scope="col">TITLE</th>
                <sec:authorize access="hasAuthority('UPDATE_ITEMS')">
                    <th scope="col">UNIQUE NUMBER</th>
                </sec:authorize>
                <th scope="col">PRICE</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${items}" var="item">
                <tr>
                    <sec:authorize access="hasAuthority('CREATE_ITEMS')">
                        <td>
                            <input type="checkbox" name="ids" value="${item.id}"/>
                        </td>
                    </sec:authorize>
                    <td>
                        <sec:authorize access="hasAuthority('CREATE_ORDERS')">
                            <a href="${pageContext.request.contextPath}/orders/create?item=${item.id}">${item.name}</a>
                        </sec:authorize>
                        <sec:authorize access="hasAuthority('UPDATE_ITEMS')">
                            <a href="${pageContext.request.contextPath}/items/${item.id}">${item.name}</a>
                        </sec:authorize>
                    </td>
                    <sec:authorize access="hasAuthority('UPDATE_ITEMS')">
                        <td>${item.uniqueNumber}</td>
                    </sec:authorize>
                    <td>${item.price}</td>

                </tr>
            </c:forEach>
            </tbody>
        </table>
        <sec:authorize access="hasAuthority('CREATE_ITEMS')">
        <div class="col-top-bottom-padding-15px">
            <input type="submit" class="btn btn-primary" aria-pressed="true" value="DELETE">
        </div>
    </form>
    </sec:authorize>

    <c:set var="entity" value="items" scope="request"/>
    <jsp:include page="util/pagination.jsp"/>

</div>

<jsp:include page="util/js.jsp"/>
</body>
</html>