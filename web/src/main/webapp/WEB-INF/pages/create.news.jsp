<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Creating news page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <c:set var="active" value="news" scope="request"></c:set>
    <jsp:include page="util/menu.jsp"/>

    <div class="col-top-bottom-padding-15px">
        <h3>Create news</h3>
    </div>

    <form:form action="${pageContext.request.contextPath}/news/create" modelAttribute="news"
               method="post">
        <form:errors path="*" cssClass="errorblock" element="div"/>
    <div class="form-group">
        <form:label path="title">Title</form:label>
        <form:input path="title" class="form-control" placeholder="Title"/>
    </div>
    <div class="form-group">
        <form:label path="content">Content</form:label>
        <form:textarea path="content" class="form-control" placeholder="Content"/>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
    </form:form>

    <jsp:include page="util/js.jsp"/>
</body>
</html>