<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Orders page for Customer</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <c:set var="active" value="orders" scope="request"></c:set>
    <jsp:include page="util/menu.jsp"/>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">TIME OF ORDER</th>
            <th scope="col">QUANTITY</th>
            <th scope="col">ITEM</th>
            <th scope="col">TOTAL PRICE</th>
            <th scope="col">STATUS</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${orders}" var="order">
            <tr>
                <td>${order.created}</td>
                <td>${order.quantity}</td>
                <td>${order.itemTitle}</td>
                <td>${order.totalPrice}</td>
                <td>${order.status}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <c:set var="entity" value="orders/user" scope="request"/>
    <jsp:include page="util/pagination.jsp"/>

</div>


<jsp:include page="util/js.jsp"/>
</body>
</html>