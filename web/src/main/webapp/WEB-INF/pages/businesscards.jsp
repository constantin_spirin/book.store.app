<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Business Cards page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>



    <form action="/businesscards/delete" method="post">
        <a href="${pageContext.request.contextPath}/businesscards/create" class="btn btn-primary"
           aria-pressed="true" role="button">CREATE</a>
        <input type="submit" class="btn btn-primary" aria-pressed="true" value="DELETE">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Full Name</th>
                <th scope="col">Working Telephone</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${businessCards}" var="businesscard">
                <tr>
                    <th scope="row"><input type="checkbox" name="ids" value="${businesscard.id}"></th>
                    <td>${businesscard.title}</td>
                    <td>${businesscard.fullName}</td>
                    <td>${businesscard.workingTelephone}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </form>

    <c:set var="entity" value="businesscards" scope="request"/>
    <jsp:include page="util/pagination.jsp"/>

</div>
<jsp:include page="util/js.jsp"/>
</body>
</html>