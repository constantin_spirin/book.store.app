<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Update item page</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <c:set var="active" value="items" scope="request"></c:set>
    <jsp:include page="util/menu.jsp"/>

    <div class="col-top-bottom-padding-15px col-12">
        <h3>Edit item</h3>
    </div>

    <form:form action="${pageContext.request.contextPath}/items/${item.id}" modelAttribute="item"
               method="post">
        <form:errors path="*" cssClass="errorblock" element="div"/>
    <div class="form-group col-12">
        <form:label path="name">Title</form:label>
        <form:input path="name" class="form-control" placeholder="${item.name}"/>
    </div>
    <div class="form-group col-3">
        <form:label path="uniqueNumber">Unique number</form:label>
        <form:input path="uniqueNumber" class="form-control" placeholder="${item.uniqueNumber}"/>
    </div>
    <div class="form-group col-12">
        <form:label path="description">Description</form:label>
        <form:textarea path="description" class="form-control" placeholder="${item.description}"/>
    </div>
    <div class="form-group col-3">
        <form:label path="price">Price</form:label>
        <form:input path="price" class="form-control" placeholder="${item.price}"/>
    </div>
    <div class="col-4">
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    </form:form>


    <jsp:include page="util/js.jsp"/>
</body>
</html>