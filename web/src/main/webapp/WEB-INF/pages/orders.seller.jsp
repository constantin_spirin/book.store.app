<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Orders page for Seller</title>
</head>
<body>
<div class="container">
    <jsp:include page="util/logo.jsp"/>

    <c:set var="active" value="orders" scope="request"></c:set>
    <jsp:include page="util/menu.jsp"/>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">TIME OF ORDER</th>
            <th scope="col">QUANTITY</th>
            <th scope="col">ITEM</th>
            <th scope="col">TOTAL PRICE</th>
            <th scope="col">STATUS</th>
            <th scope="col">CHANGE STATUS</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${orders}" var="order">
            <c:set var="anOrder" value="${order.orderForCustomerDTO}"/>
            <tr>
                <td>${anOrder.created}</td>
                <td>${anOrder.quantity}</td>
                <td>${anOrder.itemTitle}</td>
                <td>${anOrder.totalPrice}</td>
                <td>${anOrder.status}</td>
                <td>
                    <form:form action="${pageContext.request.contextPath}/orders/update" modelAttribute="changeStatus"
                               method="post">
                        <input name="userId" type="hidden" value="${order.userId}"/>
                        <input name="itemId" type="hidden" value="${order.itemId}"/>
                        <form:select path="status" items="${statuses}"/>
                        <button type="submit" class="btn btn-primary">Change</button>
                    </form:form>
                </td>

            </tr>
        </c:forEach>
        </tbody>
    </table>

    <c:set var="entity" value="orders" scope="request"/>
    <jsp:include page="util/pagination.jsp"/>

</div>

<jsp:include page="util/js.jsp"/>
</body>
</html>