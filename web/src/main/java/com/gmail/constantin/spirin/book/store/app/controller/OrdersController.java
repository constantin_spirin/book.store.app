package com.gmail.constantin.spirin.book.store.app.controller;

import com.gmail.constantin.spirin.book.store.app.controller.properties.PageProperties;
import com.gmail.constantin.spirin.book.store.app.controller.validator.ChangeOrderStatusValidator;
import com.gmail.constantin.spirin.book.store.app.controller.validator.OrderValidator;
import com.gmail.constantin.spirin.book.store.app.service.OrderService;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewOrderInfo;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderForCustomerDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderForSellerDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderIdStatusDTO;
import com.gmail.constantin.spirin.book.store.app.service.enums.StatusType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/orders")
public class OrdersController {

    private final OrderService orderService;
    private final PageProperties pageProperties;
    private final OrderValidator orderValidator;
    private final ChangeOrderStatusValidator changeOrderStatusValidator;

    @Autowired
    public OrdersController(
            PageProperties pageProperties,
            OrderService orderService,
            OrderValidator orderValidator,
            ChangeOrderStatusValidator changeOrderStatusValidator
    ) {
        this.orderService = orderService;
        this.pageProperties = pageProperties;
        this.orderValidator = orderValidator;
        this.changeOrderStatusValidator = changeOrderStatusValidator;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('CREATE_ITEMS')")
    public String getOrders(ModelMap modelMap) {
        List<OrderForSellerDTO> orders = orderService.getOrders();
        modelMap.addAttribute("orders", orders);
        modelMap.addAttribute("changeStatus", new OrderIdStatusDTO());
        modelMap.addAttribute("statuses", StatusType.values());
        return pageProperties.getOrdersSellerPagePath();
    }

    @GetMapping("/user")
    @PreAuthorize("hasAuthority('CREATE_ORDERS')")
    public String getOrdersByUser(ModelMap modelMap) {
        List<OrderForCustomerDTO> orders = orderService.getOrdersByUser();
        modelMap.addAttribute("orders", orders);
        return pageProperties.getOrdersPagePath();
    }

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('CREATE_ORDERS')")
    public String addOrderPage(
            @RequestParam("item") Long itemId,
            ModelMap modelMap
    ) {
        modelMap.addAttribute("order", orderService.getNewOrderInfo(itemId));
        return pageProperties.getCreateOrderPagePath();
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('CREATE_ORDERS')")
    public String createOrder(
            @ModelAttribute("order") NewOrderInfo orderInfo,
            BindingResult result,
            ModelMap modelMap
    ) {
        orderValidator.validate(orderInfo, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("order", orderInfo);
            return pageProperties.getCreateOrderPagePath();
        } else {
            orderService.save(orderInfo);
            return "redirect:/" + pageProperties.getOrdersPagePath() + "/user";
        }
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('CHANGE_STATUS_ORDERS')")
    public String changeStatusOrder(
            @ModelAttribute OrderIdStatusDTO changeStatus,
            BindingResult result,
            ModelMap modelMap
    ) {
        changeOrderStatusValidator.validate(changeStatus, result);
        if (!result.hasErrors()) {
            orderService.changeStatus(changeStatus);
        }
        return "redirect:/" + pageProperties.getOrdersPagePath();
    }
}
