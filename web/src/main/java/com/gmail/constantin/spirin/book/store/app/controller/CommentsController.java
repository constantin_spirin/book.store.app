package com.gmail.constantin.spirin.book.store.app.controller;

import com.gmail.constantin.spirin.book.store.app.controller.properties.PageProperties;
import com.gmail.constantin.spirin.book.store.app.controller.validator.CommentValidator;
import com.gmail.constantin.spirin.book.store.app.service.CommentService;
import com.gmail.constantin.spirin.book.store.app.service.dto.CommentDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/news")
public class CommentsController {

    private final CommentValidator commentValidator;
    private final PageProperties pageProperties;
    private final CommentService commentService;

    public CommentsController(
            CommentValidator commentValidator,
            PageProperties pageProperties,
            CommentService commentService
    ) {
        this.commentValidator = commentValidator;
        this.pageProperties = pageProperties;
        this.commentService = commentService;
    }

    @PostMapping(value = "/{newsId}/comments/create")
    @PreAuthorize("hasAuthority('CREATE_COMMENTS')")
    public String createComment(
            @PathVariable("newsId") Long newsId,
            @ModelAttribute("comment") CommentDTO comment,
            BindingResult result,
            ModelMap modelMap
    ) {
        commentValidator.validate(comment, result);
        if (result.hasErrors()) {
            // TODO set error and add to modelMap
            modelMap.addAttribute("comment", comment);
            return pageProperties.getaNewsPagePath();
        } else {
            commentService.save(comment);
            return "redirect:/news/" + newsId;
        }
    }

    @PostMapping("/{newsId}/comments/delete")
    @PreAuthorize("hasAuthority('DELETE_COMMENTS')")
    public String deleteNews(
            @PathVariable("newsId") Long newsId,
            @RequestParam("ids") Long[] ids
    ) {
        for (Long id : ids) {
            commentService.delete(id);
        }
        return "redirect:/news/" + newsId;
    }
}
