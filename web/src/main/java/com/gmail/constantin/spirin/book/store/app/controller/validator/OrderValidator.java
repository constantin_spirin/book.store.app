package com.gmail.constantin.spirin.book.store.app.controller.validator;

import com.gmail.constantin.spirin.book.store.app.service.dto.NewOrderInfo;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component("orderValidator")
public class OrderValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return NewOrderInfo.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

    }
}
