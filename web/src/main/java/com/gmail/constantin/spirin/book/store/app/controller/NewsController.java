package com.gmail.constantin.spirin.book.store.app.controller;

import com.gmail.constantin.spirin.book.store.app.controller.properties.PageProperties;
import com.gmail.constantin.spirin.book.store.app.controller.validator.NewsValidator;
import com.gmail.constantin.spirin.book.store.app.service.CommentService;
import com.gmail.constantin.spirin.book.store.app.service.NewsService;
import com.gmail.constantin.spirin.book.store.app.service.dto.CommentDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewsDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewsInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/news")
public class NewsController {

    private final PageProperties pageProperties;
    private final NewsService newsService;
    private final NewsValidator newsValidator;
    private final CommentService commentService;

    public NewsController(
            PageProperties pageProperties,
            NewsService newsService,
            NewsValidator newsValidator,
            CommentService commentService
    ) {
        this.pageProperties = pageProperties;
        this.newsService = newsService;
        this.newsValidator = newsValidator;
        this.commentService = commentService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('VIEW_NEWS')")
    public String getNews(
            @RequestParam(value = "page", defaultValue = "0") int page,
            ModelMap modelMap
    ) {
        int pagesCount = newsService.getPagesCount();
        List<NewsInfo> news = newsService.findAll(page);
        modelMap.addAttribute("news", news);
        modelMap.addAttribute("pagesCount", pagesCount);
        modelMap.addAttribute("page", page);
        return pageProperties.getNewsPagePath();
    }

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('CREATE_NEWS')")
    public String addNewsPage(ModelMap modelMap) {
        modelMap.addAttribute("news", new NewsDTO());
        return pageProperties.getCreateNewsPagePath();
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('CREATE_NEWS')")
    public String createNews(
            @ModelAttribute("news") NewsDTO news,
            BindingResult result,
            ModelMap modelMap
    ) {
        newsValidator.validate(news, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("news", news);
            return pageProperties.getCreateNewsPagePath();
        } else {
            newsService.save(news);
            return "redirect:/" + pageProperties.getNewsPagePath();
        }
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('VIEW_NEWS')")
    public String viewNews(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @PathVariable("id") Long id,
            ModelMap modelMap
    ) {
        int pagesCount = commentService.getPagesCount();
        modelMap.addAttribute("pagesCount", pagesCount);
        modelMap.addAttribute("page", page);
        modelMap.addAttribute("news", newsService.findById(id));
        modelMap.addAttribute("comment", new CommentDTO());
        modelMap.addAttribute("comments", commentService.findByNews(page, id));
        return pageProperties.getaNewsPagePath();
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('DELETE_NEWS')")
    public String deleteNews(
            @RequestParam("ids") Long[] ids
    ) {
        for (Long id : ids) {
            newsService.delete(id);
        }
        return "redirect:/" + pageProperties.getNewsPagePath();
    }

    @GetMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('UPDATE_NEWS')")
    public String updateNewsPage(
            @PathVariable("id") Long id,
            ModelMap modelMap
    ) {
        modelMap.addAttribute("news", newsService.findById(id));
        return pageProperties.getUpdateNewsPagePath();
    }

    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('UPDATE_NEWS')")
    public String updateNews(
            @PathVariable("id") Long id,
            @ModelAttribute NewsDTO news,
            BindingResult result,
            ModelMap modelMap
    ) {
        newsValidator.validate(news, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("news", news);
            return pageProperties.getUpdateNewsPagePath();
        } else {
            newsService.update(news, id);
            return "redirect:/" + pageProperties.getNewsPagePath();
        }
    }
}
