package com.gmail.constantin.spirin.book.store.app.controller.validator;

import com.gmail.constantin.spirin.book.store.app.controller.properties.ApplicationProperties;
import com.gmail.constantin.spirin.book.store.app.service.dto.MultipartFileDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;

@Component("itemsXMLValidator")
public class ItemsXMLValidator implements Validator {

    private final ApplicationProperties applicationProperties;

    @Autowired
    public ItemsXMLValidator(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return MultipartFileDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "multipartFile", "items.file.empty");

        MultipartFileDTO multipartFileDTO = (MultipartFileDTO) o;
        MultipartFile multipartFile = multipartFileDTO.getMultipartFile();

        if (!validateXml(multipartFile)) {
            errors.rejectValue("multipartFile", "items.file.invalid");
        }
    }


    public boolean validateXml(MultipartFile multipartFile) {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            File schemaFile = new File(
                    getClass().getResource("/").getPath()
                            + applicationProperties.getItemsValidationSchemaFilePath()
            );
            Schema schema = schemaFactory.newSchema(schemaFile);
            javax.xml.validation.Validator validator = schema.newValidator();
            validator.validate(new StreamSource(multipartFile.getInputStream()));
            return true;
        } catch (SAXException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}