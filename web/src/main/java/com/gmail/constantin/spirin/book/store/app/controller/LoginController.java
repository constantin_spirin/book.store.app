package com.gmail.constantin.spirin.book.store.app.controller;

import com.gmail.constantin.spirin.book.store.app.controller.properties.PageProperties;
import com.gmail.constantin.spirin.book.store.app.controller.validator.UserValidator;
import com.gmail.constantin.spirin.book.store.app.service.UserService;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserAuthData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    private final PageProperties pageProperties;

    @Autowired
    public LoginController(PageProperties pageProperties, UserValidator userValidator, UserService userService) {
        this.pageProperties = pageProperties;
    }

    @GetMapping
    public String getLoginPage(ModelMap modelMap) {
        modelMap.addAttribute("user", new UserAuthData());
        return pageProperties.getLoginPagePath();
    }
}
