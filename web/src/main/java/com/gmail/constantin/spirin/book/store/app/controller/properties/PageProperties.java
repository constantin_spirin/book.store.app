package com.gmail.constantin.spirin.book.store.app.controller.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PageProperties {

    private final Environment environment;

    private String loginPagePath;
    private String usersPagePath;
    private String errorsPagePath;
    private String itemsPagePath;
    private String createItemPagePath;
    private String updateItemPagePath;
    private String ordersPagePath;
    private String createUserPagePath;
    private String businessCardsPagePath;
    private String createBusinessCardPagePath;
    private String createOrderPagePath;
    private String ordersSellerPagePath;
    private String userForAdminPagePath;
    private String registerUserPagePath;
    private String newsPagePath;
    private String createNewsPagePath;
    private String aNewsPagePath;
    private String updateNewsPagePath;

    @Autowired
    public PageProperties(Environment environment) {
        this.environment = environment;
    }

    @PostConstruct
    public void initialize() {
        this.loginPagePath = environment.getProperty("login.page.path");
        this.usersPagePath = environment.getProperty("users.page.path");
        this.errorsPagePath = environment.getProperty("errors.page.path");
        this.itemsPagePath = environment.getProperty("items.page.path");
        this.ordersPagePath = environment.getProperty("orders.page.path");
        this.createUserPagePath = environment.getProperty("create.user.page.path");
        this.businessCardsPagePath = environment.getProperty("businesscards.page.path");
        this.createBusinessCardPagePath = environment.getProperty("create.businesscard.page.path");
        this.createItemPagePath = environment.getProperty("create.item.page.path");
        this.updateItemPagePath = environment.getProperty("update.item.page.path");
        this.createOrderPagePath = environment.getProperty("create.order.page.path");
        this.ordersSellerPagePath = environment.getProperty("orders.seller.page.path");
        this.userForAdminPagePath = environment.getProperty("user.for.admin.page.path");
        this.registerUserPagePath = environment.getProperty("register.user.page.path");
        this.newsPagePath = environment.getProperty("news.page.path");
        this.createNewsPagePath = environment.getProperty("create.news.page.path");
        this.aNewsPagePath = environment.getProperty("a.news.page.path");
        this.updateNewsPagePath = environment.getProperty("update.news.page.path");
    }

    public String getRegisterUserPagePath() {
        return registerUserPagePath;
    }

    public String getLoginPagePath() {
        return loginPagePath;
    }

    public String getUsersPagePath() {
        return usersPagePath;
    }

    public String getErrorsPagePath() {
        return errorsPagePath;
    }

    public String getItemsPagePath() {
        return itemsPagePath;
    }

    public String getCreateItemPagePath() {
        return createItemPagePath;
    }

    public String getUpdateItemPagePath() {
        return updateItemPagePath;
    }

    public String getOrdersPagePath() {
        return ordersPagePath;
    }

    public String getCreateUserPagePath() {
        return createUserPagePath;
    }

    public String getBusinessCardsPagePath() {
        return businessCardsPagePath;
    }

    public String getCreateBusinessCardPagePath() {
        return createBusinessCardPagePath;
    }

    public String getCreateOrderPagePath() {
        return createOrderPagePath;
    }

    public String getOrdersSellerPagePath() {
        return ordersSellerPagePath;
    }

    public String getUserForAdminPagePath() {
        return userForAdminPagePath;
    }

    public String getNewsPagePath() {
        return newsPagePath;
    }

    public String getCreateNewsPagePath() {
        return createNewsPagePath;
    }

    public String getaNewsPagePath() {
        return aNewsPagePath;
    }

    public String getUpdateNewsPagePath() {
        return updateNewsPagePath;
    }
}
