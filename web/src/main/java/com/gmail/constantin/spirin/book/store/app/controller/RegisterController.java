package com.gmail.constantin.spirin.book.store.app.controller;

import com.gmail.constantin.spirin.book.store.app.controller.properties.PageProperties;
import com.gmail.constantin.spirin.book.store.app.controller.validator.UserValidator;
import com.gmail.constantin.spirin.book.store.app.service.UserService;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private final PageProperties pageProperties;
    private final UserValidator userValidator;
    private final UserService userService;

    @Autowired
    public RegisterController(
            PageProperties pageProperties,
            UserValidator userValidator,
            UserService userService
    ) {
        this.pageProperties = pageProperties;
        this.userValidator = userValidator;
        this.userService = userService;
    }

    @GetMapping
    public String getRegisterPage(ModelMap modelMap) {
        modelMap.addAttribute("user", new UserDTO());
        return pageProperties.getRegisterUserPagePath();
    }

    @PostMapping
    public String registerUser(
            @ModelAttribute("user") UserDTO user,
            BindingResult result,
            ModelMap modelMap
    ) {
        userValidator.validate(user, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("user", user);
            return pageProperties.getCreateUserPagePath();
        } else {
            userService.save(user);
            return "redirect:/" + pageProperties.getItemsPagePath();
        }
    }
}
