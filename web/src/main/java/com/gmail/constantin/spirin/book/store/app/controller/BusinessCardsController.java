package com.gmail.constantin.spirin.book.store.app.controller;

import com.gmail.constantin.spirin.book.store.app.controller.properties.PageProperties;
import com.gmail.constantin.spirin.book.store.app.controller.validator.BusinessCardValidator;
import com.gmail.constantin.spirin.book.store.app.service.BusinessCardService;
import com.gmail.constantin.spirin.book.store.app.service.dto.BusinessCardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/businesscards")
public class BusinessCardsController {

    private final BusinessCardService businessCardService;
    private final PageProperties pageProperties;
    private final BusinessCardValidator businessCardValidator;

    @Autowired
    public BusinessCardsController(
            PageProperties pageProperties,
            BusinessCardService businessCardService,
            BusinessCardValidator businessCardValidator
    ) {
        this.businessCardService = businessCardService;
        this.pageProperties = pageProperties;
        this.businessCardValidator = businessCardValidator;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('MANAGE_BUSINESS_CARD')")
    public String getBusinessCards(ModelMap modelMap) {
        List<BusinessCardDTO> businessCards = businessCardService.findByUser();
        modelMap.addAttribute("businessCards", businessCards);
        return pageProperties.getBusinessCardsPagePath();
    }

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('MANAGE_BUSINESS_CARD')")
    public String addBusinessCardPage(ModelMap modelMap) {
        modelMap.addAttribute("businessCard", new BusinessCardDTO());
        return pageProperties.getCreateBusinessCardPagePath();
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('MANAGE_BUSINESS_CARD')")
    public String createBusinessCard(
            @ModelAttribute("businessCard") BusinessCardDTO businessCard,
            BindingResult result,
            ModelMap modelMap
    ) {
        businessCardValidator.validate(businessCard, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("businessCard", businessCard);
            return pageProperties.getCreateBusinessCardPagePath();
        } else {
            businessCardService.save(businessCard);
            return "redirect:/" + pageProperties.getBusinessCardsPagePath();
        }
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('MANAGE_BUSINESS_CARD')")
    public String deleteBusinessCards(
            @RequestParam("ids") Long[] ids
    ) {
        for (Long id : ids) {
            businessCardService.delete(id);
        }
        return "redirect:/" + pageProperties.getBusinessCardsPagePath();
    }
}

