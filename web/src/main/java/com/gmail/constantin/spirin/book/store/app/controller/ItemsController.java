package com.gmail.constantin.spirin.book.store.app.controller;

import com.gmail.constantin.spirin.book.store.app.controller.properties.PageProperties;
import com.gmail.constantin.spirin.book.store.app.controller.validator.ItemValidator;
import com.gmail.constantin.spirin.book.store.app.controller.validator.ItemsXMLValidator;
import com.gmail.constantin.spirin.book.store.app.service.ItemService;
import com.gmail.constantin.spirin.book.store.app.service.dto.ItemDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.MultipartFileDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/items")
public class ItemsController {

    private final ItemService itemService;
    private final PageProperties pageProperties;
    private final ItemValidator itemValidator;
    private final ItemsXMLValidator itemsXMLValidator;

    @Autowired
    public ItemsController(
            PageProperties pageProperties,
            ItemService itemService,
            ItemValidator itemValidator,
            ItemsXMLValidator itemsXMLValidator
    ) {
        this.itemService = itemService;
        this.pageProperties = pageProperties;
        this.itemValidator = itemValidator;
        this.itemsXMLValidator = itemsXMLValidator;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('VIEW_ITEMS')")
    public String getItems(
            @RequestParam(value = "page", defaultValue = "0") int page,
            ModelMap modelMap
    ) {
        int pagesCount = itemService.getPagesCount();
        List<ItemDTO> items = itemService.findAll(page);
        modelMap.addAttribute("items", items);
        modelMap.addAttribute("pagesCount", pagesCount);
        modelMap.addAttribute("page", page);
        modelMap.addAttribute("file", new MultipartFileDTO());
        return pageProperties.getItemsPagePath();
    }

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('CREATE_ITEMS')")
    public String addItemPage(ModelMap modelMap) {
        modelMap.addAttribute("item", new ItemDTO());
        return pageProperties.getCreateItemPagePath();
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('CREATE_ITEMS')")
    public String createItem(
            @ModelAttribute("item") ItemDTO item,
            BindingResult result,
            ModelMap modelMap
    ) {
        itemValidator.validate(item, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("item", item);
            return pageProperties.getCreateItemPagePath();
        } else {
            itemService.save(item);
            return "redirect:/" + pageProperties.getItemsPagePath();
        }
    }

    @PostMapping("/upload")
    @PreAuthorize("hasAuthority('CREATE_ITEMS')")
    public String uploadFile(
            @ModelAttribute("file") MultipartFileDTO multipartFile,
            BindingResult result,
            ModelMap modelMap
    ) {
        itemsXMLValidator.validate(multipartFile, result);
        if (result.hasErrors()) {
            // TODO transfer error message to view
            modelMap.addAttribute("error");
            return pageProperties.getItemsPagePath();
        } else {
            itemService.saveItemsFromXml(multipartFile);
            return "redirect:/" + pageProperties.getItemsPagePath();
        }
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('DELETE_ITEMS')")
    public String deleteItems(
            @RequestParam("ids") Long[] ids
    ) {
        for (Long id : ids) {
            itemService.delete(id);
        }
        return "redirect:/" + pageProperties.getItemsPagePath();
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('UPDATE_ITEMS')")
    public String updateItemPage(
            @PathVariable("id") Long id,
            ModelMap modelMap
    ) {
        modelMap.addAttribute("item", itemService.findById(id));
        return pageProperties.getUpdateItemPagePath();
    }

    @PostMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('UPDATE_ITEMS')")
    public String updateItem(
            @PathVariable("id") Long id,
            @ModelAttribute ItemDTO item,
            BindingResult result,
            ModelMap modelMap
    ) {
        itemValidator.validate(item, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("item", item);
            return pageProperties.getUpdateItemPagePath();
        } else {
            itemService.update(item, id);
            return "redirect:/" + pageProperties.getItemsPagePath();
        }
    }
}
