package com.gmail.constantin.spirin.book.store.app.controller.validator;

import com.gmail.constantin.spirin.book.store.app.service.dto.UserDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

@Component("passwordValidator")
public class PasswordValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "password", "password.empty");
        UserDTO user = (UserDTO) o;

        Pattern pattern = Pattern.compile(
                "^[A-Z0-9._%+_]+$",
                Pattern.CASE_INSENSITIVE
        );
        if (!(pattern.matcher(user.getPassword()).matches())) {
            errors.rejectValue("email", "user.password.invalid");
        }
    }
}
