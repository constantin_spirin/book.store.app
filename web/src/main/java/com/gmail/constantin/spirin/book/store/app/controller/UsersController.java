package com.gmail.constantin.spirin.book.store.app.controller;

import com.gmail.constantin.spirin.book.store.app.controller.properties.PageProperties;
import com.gmail.constantin.spirin.book.store.app.controller.validator.PasswordValidator;
import com.gmail.constantin.spirin.book.store.app.controller.validator.RoleValidator;
import com.gmail.constantin.spirin.book.store.app.controller.validator.UserValidator;
import com.gmail.constantin.spirin.book.store.app.service.RoleService;
import com.gmail.constantin.spirin.book.store.app.service.UserService;
import com.gmail.constantin.spirin.book.store.app.service.dto.RoleDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UsersController {

    private final PageProperties pageProperties;
    private final UserService userService;
    private final RoleService roleService;
    private final UserValidator userValidator;
    private final PasswordValidator passwordValidator;
    private final RoleValidator roleValidator;


    @Autowired
    public UsersController(
            PageProperties pageProperties,
            UserService userService,
            RoleService roleService,
            UserValidator userValidator,
            PasswordValidator passwordValidator,
            RoleValidator roleValidator
    ) {
        this.pageProperties = pageProperties;
        this.userService = userService;
        this.roleService = roleService;
        this.userValidator = userValidator;
        this.passwordValidator = passwordValidator;
        this.roleValidator = roleValidator;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('VIEW_USERS')")
    public String getUsers(
            @RequestParam(value = "page", defaultValue = "0") int page,
            ModelMap modelMap
    ) {
        List<UserInfo> users = userService.findUsersInfo(page);
        modelMap.addAttribute("users", users);
        return pageProperties.getUsersPagePath();
    }

    @GetMapping(value = "/create")
    @PreAuthorize("hasAuthority('CREATE_USERS')")
    public String addUserPage(ModelMap modelMap) {
        List<RoleDTO> roles = roleService.getAll();
        modelMap.addAttribute("roles", roles);
        modelMap.addAttribute("user", new UserDTO());
        return pageProperties.getCreateUserPagePath();
    }

    @PostMapping
    @PreAuthorize("hasAuthority('CREATE_USERS')")
    public String createUser(
            @ModelAttribute("user") UserDTO user,
            BindingResult result,
            ModelMap modelMap
    ) {
        userValidator.validate(user, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("user", user);
            return pageProperties.getCreateUserPagePath();
        } else {
            userService.save(user);
            return pageProperties.getUsersPagePath();
        }
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('CHANGE_ROLE_PASSWORD_USERS')")
    public String getUserForUpdate(
            @PathVariable("id") Long id,
            ModelMap modelMap
    ) {
        UserDTO user = userService.findById(id);
        modelMap.addAttribute("user", user);
        List<RoleDTO> roles = roleService.getAll();
        modelMap.addAttribute("roles", roles);
        ;
        return pageProperties.getUserForAdminPagePath();
    }

    @PostMapping(value = "/updatepassword")
    @PreAuthorize("hasAuthority('CHANGE_ROLE_PASSWORD_USERS')")
    public String updatePassword(
            @ModelAttribute("user") UserDTO user,
            BindingResult result,
            ModelMap modelMap
    ) {
        passwordValidator.validate(user, result);
        if (result.hasErrors()) {
            List<RoleDTO> roles = roleService.getAll();
            modelMap.addAttribute("roles", roles);
            modelMap.addAttribute("user", user);
            return pageProperties.getUserForAdminPagePath();
        } else {
            userService.updatePassword(user);
            return "redirect:/" + pageProperties.getUsersPagePath();
        }
    }

    @PostMapping(value = "/updaterole")
    @PreAuthorize("hasAuthority('CHANGE_ROLE_PASSWORD_USERS')")
    public String updateRole(
            @ModelAttribute("user") UserDTO user,
            BindingResult result,
            ModelMap modelMap
    ) {
        roleValidator.validate(user, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("user", user);
            return pageProperties.getUserForAdminPagePath();
        } else {
            userService.updateRole(user);
            return "redirect:/" + pageProperties.getUsersPagePath();
        }
    }

    @PostMapping("/disable")
    @PreAuthorize("hasAuthority('DISABLE_USERS')")
    public String disableUsers(
            @RequestParam("ids") Long[] ids
    ) {
        for (Long id : ids) {
            userService.disableUser(id);
        }
        return "redirect:/" + pageProperties.getUsersPagePath();
    }

    @PostMapping("/enable")
    @PreAuthorize("hasAuthority('DISABLE_USERS')")
    public String enableUsers(
            @RequestParam("ids") Long[] ids
    ) {
        for (Long id : ids) {
            userService.enableUser(id);
        }
        return "redirect:/" + pageProperties.getUsersPagePath();
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('DELETE_USERS')")
    public String deleteUsers(
            @RequestParam("ids") Long[] ids
    ) {
        for (Long id : ids) {
            userService.deleteUser(id);
        }
        return "redirect:/" + pageProperties.getUsersPagePath();
    }

    @PostMapping("/restore")
    @PreAuthorize("hasAuthority('DELETE_USERS')")
    public String restoreUsers(
            @RequestParam("ids") Long[] ids
    ) {
        for (Long id : ids) {
            userService.restoreUser(id);
        }
        return "redirect:/" + pageProperties.getUsersPagePath();
    }
}
