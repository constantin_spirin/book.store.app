package com.gmail.constantin.spirin.book.store.app.controller;

import com.gmail.constantin.spirin.book.store.app.controller.properties.PageProperties;
import com.gmail.constantin.spirin.book.store.app.controller.validator.UserValidator;
import com.gmail.constantin.spirin.book.store.app.service.UserService;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserController {

    private final PageProperties pageProperties;
    private final UserService userService;
    private final UserValidator userValidator;

    public UserController(PageProperties pageProperties, UserService userService, UserValidator userValidator) {
        this.pageProperties = pageProperties;
        this.userService = userService;
        this.userValidator = userValidator;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('CREATE_ORDERS')")
    public String getEditProfilePage(ModelMap modelMap) {
        UserDTO user = userService.getCurrentUserDTO();
        modelMap.addAttribute("user", user);
        return pageProperties.getUsersPagePath();
    }
}
