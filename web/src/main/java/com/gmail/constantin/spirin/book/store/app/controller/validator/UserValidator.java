package com.gmail.constantin.spirin.book.store.app.controller.validator;

import com.gmail.constantin.spirin.book.store.app.service.UserService;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

@Component("userValidator")
public class UserValidator implements Validator {

    private final UserService userService;

    @Autowired
    public UserValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "user.name.empty");
        ValidationUtils.rejectIfEmpty(errors, "email", "user.email.empty");
        UserDTO user = (UserDTO) o;

        Pattern pattern = Pattern.compile(
                "^[A-Z0-9._%+_]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
                Pattern.CASE_INSENSITIVE
        );
        if (!(pattern.matcher(user.getEmail()).matches())) {
            errors.rejectValue("email", "user.email.invalid");
        }

        if (userService.findUserByEmail(user.getEmail()) != null) {
            errors.rejectValue("email", "user.email.already.exists");
        }
    }

}
