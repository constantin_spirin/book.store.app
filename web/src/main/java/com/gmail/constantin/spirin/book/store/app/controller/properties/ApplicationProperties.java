package com.gmail.constantin.spirin.book.store.app.controller.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ApplicationProperties {

    private final Environment environment;

    private String itemsValidationSchemaFilePath;
    private String xmlFilesDirectoryPath;

    @Autowired
    public ApplicationProperties(Environment environment) {
        this.environment = environment;
    }

    @PostConstruct
    public void initialize() {
        this.itemsValidationSchemaFilePath = environment.getProperty("upload.items.xml.schema.file.path");
        this.xmlFilesDirectoryPath = environment.getProperty("xml.files.directory.path");
    }

    public String getItemsValidationSchemaFilePath() {
        return itemsValidationSchemaFilePath;
    }

    public String getXmlFilesDirectoryPath() {
        return xmlFilesDirectoryPath;
    }
}
