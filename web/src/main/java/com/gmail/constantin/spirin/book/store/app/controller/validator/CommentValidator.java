package com.gmail.constantin.spirin.book.store.app.controller.validator;

import com.gmail.constantin.spirin.book.store.app.service.dto.CommentDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component("commentsValidator")
public class CommentValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return CommentDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "content", "comment.content.empty");
    }
}
