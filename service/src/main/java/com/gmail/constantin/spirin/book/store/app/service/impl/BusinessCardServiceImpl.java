package com.gmail.constantin.spirin.book.store.app.service.impl;

import com.gmail.constantin.spirin.book.store.app.service.converter.dto.DTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.entity.Converter;
import com.gmail.constantin.spirin.book.store.app.service.util.ServiceUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.gmail.constantin.spirin.book.store.app.dao.BusinessCardDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.BusinessCard;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import com.gmail.constantin.spirin.book.store.app.service.BusinessCardService;
import com.gmail.constantin.spirin.book.store.app.service.dto.BusinessCardDTO;

import javax.transaction.Transactional;
import java.util.List;

@Service("businessCardService")
@Transactional
public class BusinessCardServiceImpl implements BusinessCardService {

    private static final Logger logger = LogManager.getLogger(BusinessCardServiceImpl.class);

    private final Converter<BusinessCardDTO, BusinessCard> businessCardConverter;
    private final DTOConverter<BusinessCardDTO, BusinessCard> businessCardDTOConverter;
    private final BusinessCardDao businessCardDao;
    private final ServiceUtil serviceUtil;

    @Autowired
    public BusinessCardServiceImpl(
            @Qualifier("businessCardConverter") Converter<BusinessCardDTO, BusinessCard> businessCardConverter,
            @Qualifier("businessCardDTOConverter") DTOConverter<BusinessCardDTO, BusinessCard> businessCardDTOConverter,
            @Qualifier("businessCardDao") BusinessCardDao businessCardDao,
            @Qualifier("serviceUtil") ServiceUtil serviceUtil
    ) {
        this.businessCardConverter = businessCardConverter;
        this.businessCardDTOConverter = businessCardDTOConverter;
        this.businessCardDao = businessCardDao;
        this.serviceUtil = serviceUtil;
    }

    @Override
    public BusinessCardDTO save(BusinessCardDTO dto) {
        BusinessCard businessCard = businessCardConverter.toEntity(dto);
        User user = serviceUtil.getCurrentUser();
        user.getBusinessCards().add(businessCard);
        return businessCardDTOConverter.toDTO(businessCard);
    }

    @Override
    public List<BusinessCardDTO> findByUser() {
        User user = serviceUtil.getCurrentUser();
        return businessCardDTOConverter.toDTOList(user.getBusinessCards());
    }

    @Override
    public void delete(Long id) {
        User user = serviceUtil.getCurrentUser();
        List<BusinessCard> businessCards = user.getBusinessCards();
        BusinessCard deletingBusinessCard = businessCardDao.findOne(id);
        if (businessCards.contains(deletingBusinessCard)) {
            businessCards.remove(deletingBusinessCard);
        } else {
            logger.info("Attempt to delete user with id " + user.getId() + " business card with id " + id);
        }
    }
}
