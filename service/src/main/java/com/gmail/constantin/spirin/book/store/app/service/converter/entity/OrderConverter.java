package com.gmail.constantin.spirin.book.store.app.service.converter.entity;

import org.springframework.stereotype.Component;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Order;
import com.gmail.constantin.spirin.book.store.app.dao.entity.OrderId;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewOrderInfo;
import com.gmail.constantin.spirin.book.store.app.service.enums.StatusType;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Component("orderConverter")
public class OrderConverter implements Converter<NewOrderInfo, Order> {

    @Override
    public Order toEntity(NewOrderInfo dto) {
        Order order = new Order();
        order.setId(new OrderId(dto.getUserId(), dto.getItemId()));
        order.setCreated(dto.getCreated());
        order.setQuantity(dto.getQuantity());
        order.setStatus(StatusType.NEW.toString());
        return order;
    }

    @Override
    public List<Order> toEntityList(List<NewOrderInfo> list) {
        return list.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
