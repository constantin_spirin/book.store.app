package com.gmail.constantin.spirin.book.store.app.service.impl;

import com.gmail.constantin.spirin.book.store.app.dao.NewsDao;
import com.gmail.constantin.spirin.book.store.app.dao.UserDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.News;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import com.gmail.constantin.spirin.book.store.app.service.NewsService;
import com.gmail.constantin.spirin.book.store.app.service.converter.dto.NewsDTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.dto.NewsInfoDTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.entity.NewsConverter;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewsDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewsInfo;
import com.gmail.constantin.spirin.book.store.app.service.util.ServiceUtil;
import com.gmail.constantin.spirin.book.store.app.service.util.pagination.Pagination;
import com.gmail.constantin.spirin.book.store.app.service.util.pagination.properties.PaginationProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {

    private static final Logger logger = LogManager.getLogger(AuditServiceImpl.class);

    private final NewsDao newsDao;
    private final NewsConverter newsConverter;
    private final NewsDTOConverter newsDTOConverter;
    private final ServiceUtil serviceUtil;
    private final PaginationProperties paginationProperties;
    private final NewsInfoDTOConverter newsInfoDTOConverter;
    private final UserDao userDao;

    @Autowired
    public NewsServiceImpl(
            UserDao userDao,
            NewsDao newsDao,
            @Qualifier("newsConverter") NewsConverter newsConverter,
            @Qualifier("newsDTOConverter") NewsDTOConverter newsDTOConverter,
            ServiceUtil serviceUtil,
            PaginationProperties paginationProperties,
            @Qualifier("newsInfoDTOConverter") NewsInfoDTOConverter newsInfoDTOConverter
    ) {
        this.userDao = userDao;
        this.newsDao = newsDao;
        this.newsConverter = newsConverter;
        this.newsDTOConverter = newsDTOConverter;
        this.serviceUtil = serviceUtil;
        this.paginationProperties = paginationProperties;
        this.newsInfoDTOConverter = newsInfoDTOConverter;
    }

    @Override
    public NewsDTO save(NewsDTO dto) {
        User user = serviceUtil.getCurrentUser();
        dto.setCreated(LocalDateTime.now());
        News news = newsConverter.toEntity(dto);
        news.setUser(user);
        newsDao.create(news);
        dto.setUserId(user.getId());
        return dto;
    }

    @Override
    public List<NewsInfo> findAll(int page) {
        int limit = paginationProperties.getNewsCountOnPage();
        int first = Pagination.getFirst(page, limit);
        List<News> newsList = newsDao.findAll(first, limit);
        return newsInfoDTOConverter.toDTOList(newsList);
    }

    @Override
    public void delete(Long id) {
        News news = newsDao.findOne(id);
        news.setDeleted(true);
    }

    @Override
    public void update(NewsDTO newsDTO, Long id) {
        News news = newsDao.findOne(id);
        news.setTitle(newsDTO.getTitle());
        news.setContent(newsDTO.getContent());
        newsDao.update(news);
    }

    @Override
    public NewsDTO findById(Long id) {
        News news = newsDao.findOne(id);
        return newsDTOConverter.toDTO(news);
    }

    @Override
    public int getPagesCount() {
        int rowsCount = newsDao.getRowsCount();
        return Pagination.pagesCount(rowsCount, paginationProperties.getNewsCountOnPage());
    }
}
