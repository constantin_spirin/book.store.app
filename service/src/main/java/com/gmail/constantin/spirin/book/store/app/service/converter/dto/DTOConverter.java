package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import java.util.List;

public interface DTOConverter<D, E> {

    D toDTO(E entity);

    List<D> toDTOList(List<E> list);
}
