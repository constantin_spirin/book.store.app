package com.gmail.constantin.spirin.book.store.app.service.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import com.gmail.constantin.spirin.book.store.app.dao.UserDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserPrincipal;

@Service("serviceUtil")
public class ServiceUtil {

    private final UserDao userDao;

    @Autowired
    public ServiceUtil(@Qualifier("userDao") UserDao userDao) {
        this.userDao = userDao;
    }

    public User getCurrentUser() {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = userPrincipal.getId();
        User user = userDao.findOne(userId);
        return user;
    }
}
