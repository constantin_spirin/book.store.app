package com.gmail.constantin.spirin.book.store.app.service;

import com.gmail.constantin.spirin.book.store.app.service.dto.NewOrderInfo;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderForCustomerDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderForSellerDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderIdStatusDTO;

import java.util.List;

public interface OrderService {
    NewOrderInfo save(NewOrderInfo dto);
    List<OrderForCustomerDTO> getOrdersByUser();
    List<OrderForSellerDTO> getOrders();
    NewOrderInfo getNewOrderInfo(Long itemId);
    void changeStatus(OrderIdStatusDTO orderDTO);
}
