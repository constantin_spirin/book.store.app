package com.gmail.constantin.spirin.book.store.app.service.util.pagination;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Pagination {

    public static int getFirst(int page, int limit) {
        return page * limit;
    }

    public static int pagesCount(int rowsCount, int rowsPerPage) {
        Long numberOfPages = new BigDecimal(rowsCount).divide(new BigDecimal(rowsPerPage), RoundingMode.UP).longValue();
        return numberOfPages.intValue();
    }
}
