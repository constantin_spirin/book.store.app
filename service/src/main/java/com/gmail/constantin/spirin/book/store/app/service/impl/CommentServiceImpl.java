package com.gmail.constantin.spirin.book.store.app.service.impl;

import com.gmail.constantin.spirin.book.store.app.dao.CommentDao;
import com.gmail.constantin.spirin.book.store.app.service.converter.dto.CommentDTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.dto.CommentInfoDTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.entity.CommentConverter;
import com.gmail.constantin.spirin.book.store.app.service.dto.CommentInfo;
import com.gmail.constantin.spirin.book.store.app.service.util.ServiceUtil;
import com.gmail.constantin.spirin.book.store.app.service.util.pagination.Pagination;
import com.gmail.constantin.spirin.book.store.app.service.util.pagination.properties.PaginationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.gmail.constantin.spirin.book.store.app.dao.NewsDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Comment;
import com.gmail.constantin.spirin.book.store.app.dao.entity.News;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import com.gmail.constantin.spirin.book.store.app.service.CommentService;
import com.gmail.constantin.spirin.book.store.app.service.dto.CommentDTO;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    private final CommentConverter commentConverter;
    private final CommentDTOConverter commentDTOConverter;
    private final NewsDao newsDao;
    private final ServiceUtil serviceUtil;
    private final CommentDao commentDao;
    private final PaginationProperties paginationProperties;
    private final CommentInfoDTOConverter commentInfoDTOConverter;

    @Autowired
    public CommentServiceImpl(
            @Qualifier("commentConverter") CommentConverter commentConverter,
            @Qualifier("commentDTOConverter") CommentDTOConverter commentDTOConverter,
            @Qualifier("newsDao") NewsDao newsDao,
            ServiceUtil serviceUtil,
            CommentDao commentDao,
            PaginationProperties paginationProperties,
            CommentInfoDTOConverter commentInfoDTOConverter
    ) {
        this.commentConverter = commentConverter;
        this.commentDTOConverter = commentDTOConverter;
        this.newsDao = newsDao;
        this.serviceUtil = serviceUtil;
        this.commentDao = commentDao;
        this.paginationProperties = paginationProperties;
        this.commentInfoDTOConverter = commentInfoDTOConverter;
    }

    @Override
    public CommentDTO save(CommentDTO dto) {
        dto.setCreated(LocalDateTime.now());
        Comment comment = commentConverter.toEntity(dto);
        User user = serviceUtil.getCurrentUser();
        News news = newsDao.findOne(dto.getNewsId());
        comment.setUser(user);
        comment.setNews(news);
        commentDao.create(comment);
        return commentDTOConverter.toDTO(comment);
    }

    @Override
    public List<CommentInfo> findByNews(int page, Long newsId) {
        int limit = paginationProperties.getCommentsCountOnPage();
        int first = Pagination.getFirst(page, limit);
        News news = newsDao.findOne(newsId);
        return commentInfoDTOConverter.toDTOList(commentDao.findCommentsByNews(first, limit, news));
    }

    @Override
    public void delete(Long id) {
        Comment comment = commentDao.findOne(id);
        comment.setDeleted(true);
    }

    @Override
    public int getPagesCount() {
        int rowsCount = commentDao.getRowsCount();
        return Pagination.pagesCount(rowsCount, paginationProperties.getCommentsCountOnPage());
    }
}
