package com.gmail.constantin.spirin.book.store.app.service.impl;

import com.gmail.constantin.spirin.book.store.app.service.converter.dto.AuditDTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.entity.AuditConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.gmail.constantin.spirin.book.store.app.dao.AuditDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Audit;
import com.gmail.constantin.spirin.book.store.app.service.AuditService;
import com.gmail.constantin.spirin.book.store.app.service.dto.AuditDTO;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Service
@Transactional
public class AuditServiceImpl implements AuditService {

    private final AuditDao auditDao;
    private final AuditDTOConverter auditDTOConverter;
    private final AuditConverter auditConverter;

    @Autowired
    public AuditServiceImpl(
            @Qualifier("auditDao") AuditDao auditDao,
            @Qualifier("auditDTOConverter") AuditDTOConverter auditDTOConverter,
            @Qualifier("auditConverter") AuditConverter auditConverter
    ) {
        this.auditDao = auditDao;
        this.auditDTOConverter = auditDTOConverter;
        this.auditConverter = auditConverter;
    }

    @Override
    public AuditDTO save(AuditDTO auditDTO) {
        Audit audit = auditConverter.toEntity(auditDTO);
        audit.setCreated(LocalDateTime.now(ZoneId.systemDefault()));
        auditDao.create(audit);
        return auditDTOConverter.toDTO(audit);
    }
}