package com.gmail.constantin.spirin.book.store.app.service;

import com.gmail.constantin.spirin.book.store.app.service.dto.AuditDTO;

public interface AuditService {
    AuditDTO save(AuditDTO auditDTO);

}
