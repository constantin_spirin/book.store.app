package com.gmail.constantin.spirin.book.store.app.service;

import com.gmail.constantin.spirin.book.store.app.service.dto.UserDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserInfo;
import java.util.List;

public interface UserService {
    UserDTO save(UserDTO userDTO);

    UserDTO findUserByEmail(String email);

    List<UserDTO> findAll();

    List<UserInfo> findUsersInfo(int page);

    UserDTO findById(Long id);

    void updatePassword(UserDTO userDTO);

    void updateRole(UserDTO userDTO);

    void disableUser(Long id);

    void enableUser(Long id);

    void deleteUser(Long id);

    void restoreUser(Long id);

    UserDTO getCurrentUserDTO();
}
