package com.gmail.constantin.spirin.book.store.app.service;

import com.gmail.constantin.spirin.book.store.app.service.dto.BusinessCardDTO;

import java.util.List;

public interface BusinessCardService {

    List<BusinessCardDTO> findByUser();

    BusinessCardDTO save(BusinessCardDTO dto);

    void delete(Long id);
}
