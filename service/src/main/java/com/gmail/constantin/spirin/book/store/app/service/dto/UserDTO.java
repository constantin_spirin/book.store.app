package com.gmail.constantin.spirin.book.store.app.service.dto;

public class UserDTO {

    private Long id;
    private String email;
    private String name;
    private String surname;
    private String password;
    private Long roleId;
    private ProfileDTO profileDTO;

    public UserDTO() {
        profileDTO = new ProfileDTO();
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ProfileDTO getProfileDTO() {
        return profileDTO;
    }

    public void setProfileDTO(ProfileDTO profileDTO) {
        this.profileDTO = profileDTO;
    }

    public void setAddress(String address) {
        this.profileDTO.setAddress(address);
    }

    public void setTelephone(String telephone) {
        this.profileDTO.setTelephone(telephone);
    }

    public String getTelephone() {
        return this.getProfileDTO().getTelephone();
    }

    public String getAddress() {
        return this.getProfileDTO().getAddress();
    }
}
