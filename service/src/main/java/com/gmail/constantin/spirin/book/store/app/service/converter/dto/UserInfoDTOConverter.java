package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import com.gmail.constantin.spirin.book.store.app.dao.RoleDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Role;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserInfo;
import com.gmail.constantin.spirin.book.store.app.service.enums.RoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("userInfoDTOConverter")
public class UserInfoDTOConverter implements DTOConverter<UserInfo, User> {

    private final RoleDao roleDao;

    @Autowired
    public UserInfoDTOConverter(@Qualifier("roleDao") RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public UserInfo toDTO(User entity) {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(entity.getId());
        userInfo.setEmail(entity.getEmail());
        userInfo.setFullName(entity.getName() + " " + entity.getSurname());
        Role role = entity.getRole();
        userInfo.setRole(RoleType.valueOf(role.getName()));
        userInfo.setEnable(entity.getEnabled());
        userInfo.setDeleted(entity.getDeleted());
        return userInfo;
    }

    @Override
    public List<UserInfo> toDTOList(List<User> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
