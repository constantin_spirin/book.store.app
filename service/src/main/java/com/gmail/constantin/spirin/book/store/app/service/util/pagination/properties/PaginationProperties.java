package com.gmail.constantin.spirin.book.store.app.service.util.pagination.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PaginationProperties {

    private final Environment environment;

    private int itemsCountOnPage;
    private int usersCountOnPage;
    private int ordersCountOnPage;
    private int newsCountOnPage;
    private int commentsCountOnPage;
    private int businessCardsCountOnPage;


    @Autowired
    public PaginationProperties(Environment environment) {
        this.environment = environment;
    }

    @PostConstruct
    public void initialize() {
        this.itemsCountOnPage = Integer.parseInt(environment.getProperty("items.count.on.page"));
        this.newsCountOnPage = Integer.parseInt(environment.getProperty("news.count.on.page"));
        this.commentsCountOnPage = Integer.parseInt(environment.getProperty("comments.count.on.page"));
        this.usersCountOnPage = Integer.parseInt(environment.getProperty("users.count.on.page"));
    }

    public int getItemsCountOnPage() {
        return itemsCountOnPage;
    }

    public int getNewsCountOnPage() {
        return newsCountOnPage;
    }

    public int getCommentsCountOnPage() {
        return commentsCountOnPage;
    }

    public int getUsersCountOnPage() {
        return usersCountOnPage;
    }
}
