package com.gmail.constantin.spirin.book.store.app.service.enums;

public enum RoleType {
    CUSTOMER, SELLER, SECURITY, API
}
