package com.gmail.constantin.spirin.book.store.app.service.util.fromXmlConvert;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "store")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemsElement {

    @XmlElement(name = "item")
    private List<ItemElement> items = null;

    public List<ItemElement> getItems() {
        return items;
    }

    public void setItems(List<ItemElement> items) {
        this.items = items;
    }
}
