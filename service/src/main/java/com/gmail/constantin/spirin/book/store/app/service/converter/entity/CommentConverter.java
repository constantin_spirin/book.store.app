package com.gmail.constantin.spirin.book.store.app.service.converter.entity;

import org.springframework.stereotype.Component;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Comment;
import com.gmail.constantin.spirin.book.store.app.service.dto.CommentDTO;

import java.util.List;
import java.util.stream.Collectors;

@Component("commentConverter")
public class CommentConverter implements Converter<CommentDTO, Comment> {
    @Override
    public Comment toEntity(CommentDTO dto) {
        Comment comment = new Comment();
        comment.setId(dto.getId());
        comment.setContent(dto.getContent());
        comment.setCreated(dto.getCreated());
        return comment;
    }

    @Override
    public List<Comment> toEntityList(List<CommentDTO> list) {
        return list.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
