package com.gmail.constantin.spirin.book.store.app.service.impl;

import com.gmail.constantin.spirin.book.store.app.dao.RoleDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Role;
import com.gmail.constantin.spirin.book.store.app.service.RoleService;
import com.gmail.constantin.spirin.book.store.app.service.converter.dto.RoleDTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.entity.RoleConverter;
import com.gmail.constantin.spirin.book.store.app.service.dto.RoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    private final RoleDao roleDao;
    private final RoleDTOConverter roleDTOConverter;
    private final RoleConverter roleConverter;

    @Autowired
    public RoleServiceImpl(
            @Qualifier("roleDao") RoleDao roleDao,
            @Qualifier("roleDTOConverter") RoleDTOConverter roleDTOConverter,
            @Qualifier("roleConverter") RoleConverter roleConverter
    ) {
        this.roleDao = roleDao;
        this.roleDTOConverter = roleDTOConverter;
        this.roleConverter = roleConverter;
    }

    @Override
    public RoleDTO save(RoleDTO roleDTO) {
        Role role = roleConverter.toEntity(roleDTO);
        roleDao.create(role);
        return roleDTOConverter.toDTO(role);
    }

    @Override
    public List<RoleDTO> getAll() {
        List<RoleDTO> roles = roleDTOConverter.toDTOList(roleDao.findAll());
        return roles;
    }
}
