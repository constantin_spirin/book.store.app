package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Order;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderForCustomerDTO;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Component("orderForCustomerDTOConverter")
public class OrderForCustomerDTOConverter implements DTOConverter<OrderForCustomerDTO, Order> {
    @Override
    public OrderForCustomerDTO toDTO(Order entity) {
        OrderForCustomerDTO orderDTO = new OrderForCustomerDTO();
        orderDTO.setQuantity(entity.getQuantity());
        orderDTO.setStatus(entity.getStatus());
        orderDTO.setCreated(entity.getCreated());
        orderDTO.setItemTitle(entity.getItem().getName());
        orderDTO.setTotalPrice(entity.getItem().getPrice().multiply(new BigDecimal(entity.getQuantity())));
        return orderDTO;
    }

    @Override
    public List<OrderForCustomerDTO> toDTOList(List<Order> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
