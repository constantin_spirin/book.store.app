package com.gmail.constantin.spirin.book.store.app.service;

import com.gmail.constantin.spirin.book.store.app.service.dto.CommentDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.CommentInfo;

import java.util.List;

public interface CommentService {
    CommentDTO save(CommentDTO dto);

    List<CommentInfo> findByNews(int page, Long newsId);

    void delete(Long id);

    int getPagesCount();
}
