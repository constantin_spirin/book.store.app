package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Audit;
import com.gmail.constantin.spirin.book.store.app.service.dto.AuditDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("auditDTOConverter")
public class AuditDTOConverter implements DTOConverter<AuditDTO, Audit> {
    @Override
    public AuditDTO toDTO(Audit entity) {
        AuditDTO auditDTO = new AuditDTO();
        auditDTO.setCreated(entity.getCreated());
        auditDTO.setId(entity.getId());
        auditDTO.setEventType(entity.getEventType());
        return auditDTO;
    }

    @Override
    public List<AuditDTO> toDTOList(List<Audit> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
