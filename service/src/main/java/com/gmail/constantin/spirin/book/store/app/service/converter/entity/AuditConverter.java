package com.gmail.constantin.spirin.book.store.app.service.converter.entity;

import org.springframework.stereotype.Component;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Audit;
import com.gmail.constantin.spirin.book.store.app.service.dto.AuditDTO;

import java.util.List;
import java.util.stream.Collectors;

@Component("auditConverter")
public class AuditConverter implements Converter<AuditDTO, Audit> {
    @Override
    public Audit toEntity(AuditDTO dto) {
        Audit audit = new Audit();
        audit.setCreated(dto.getCreated());
        audit.setEventType(dto.getEventType());
        audit.setId(dto.getId());
        return audit;
    }

    @Override
    public List<Audit> toEntityList(List<AuditDTO> list) {
        return list.stream().map(this::toEntity).collect(Collectors.toList());
    }
}