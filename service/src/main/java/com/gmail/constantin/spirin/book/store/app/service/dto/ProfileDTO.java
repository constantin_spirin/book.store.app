package com.gmail.constantin.spirin.book.store.app.service.dto;

public class ProfileDTO {
    private String address;
    private String telephone;

    public ProfileDTO(){}

    public ProfileDTO(String address, String telephone) {
        this.address = address;
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
