package com.gmail.constantin.spirin.book.store.app.service;

import com.gmail.constantin.spirin.book.store.app.service.dto.MultipartFileDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.ItemDTO;

import java.util.List;

public interface ItemService {
    ItemDTO save(ItemDTO dto);

    List<ItemDTO> findAll(int page);

    int getPagesCount();

    void delete(Long id);

    void update(ItemDTO itemDTO, Long id);

    ItemDTO findById(Long id);

    List<ItemDTO> saveItemsFromXml(MultipartFileDTO multipartFileDTO);
}
