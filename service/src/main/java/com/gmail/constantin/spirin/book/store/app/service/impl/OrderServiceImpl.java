package com.gmail.constantin.spirin.book.store.app.service.impl;

import com.gmail.constantin.spirin.book.store.app.dao.ItemDao;
import com.gmail.constantin.spirin.book.store.app.dao.OrderDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Item;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Order;
import com.gmail.constantin.spirin.book.store.app.dao.entity.OrderId;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import com.gmail.constantin.spirin.book.store.app.service.OrderService;
import com.gmail.constantin.spirin.book.store.app.service.converter.dto.OrderForCustomerDTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.dto.OrderForSellerDTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.entity.OrderConverter;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewOrderInfo;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderForCustomerDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderForSellerDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderIdStatusDTO;
import com.gmail.constantin.spirin.book.store.app.service.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private final OrderDao orderDao;
    private final ItemDao itemDaoCustom;
    private final OrderForCustomerDTOConverter orderForCustomerDTOConverter;
    private final OrderForSellerDTOConverter orderForSellerDTOConverter;
    private final OrderConverter orderConverter;
    private final ServiceUtil serviceUtil;

    @Autowired
    public OrderServiceImpl(
            @Qualifier("orderDao") OrderDao orderDao,
            @Qualifier("itemDao") ItemDao itemDaoCustom,
            @Qualifier("orderForCustomerDTOConverter") OrderForCustomerDTOConverter orderForCustomerDTOConverter,
            @Qualifier("orderForSellerDTOConverter") OrderForSellerDTOConverter orderForSellerDTOConverter,
            @Qualifier("orderConverter") OrderConverter orderConverter,
            @Qualifier("serviceUtil") ServiceUtil serviceUtil
    ) {
        this.orderDao = orderDao;
        this.itemDaoCustom = itemDaoCustom;
        this.orderForCustomerDTOConverter = orderForCustomerDTOConverter;
        this.orderForSellerDTOConverter = orderForSellerDTOConverter;
        this.orderConverter = orderConverter;
        this.serviceUtil = serviceUtil;
    }


    @Override
    public NewOrderInfo save(NewOrderInfo dto) {
        dto.setUserId(serviceUtil.getCurrentUser().getId());
        dto.setCreated(LocalDateTime.now());
        Order order = orderConverter.toEntity(dto);
        orderDao.create(order);
        return dto;
    }

    @Override
    public List<OrderForCustomerDTO> getOrdersByUser() {
        User user = serviceUtil.getCurrentUser();
        return orderForCustomerDTOConverter.toDTOList(user.getOrders());
    }

    @Override
    public List<OrderForSellerDTO> getOrders() {
        List<Order> orders = orderDao.findAll();
        return orderForSellerDTOConverter.toDTOList(orders);
    }

    @Override
    public NewOrderInfo getNewOrderInfo(Long itemId) {
        Item item = itemDaoCustom.findOne(itemId);
        NewOrderInfo newOrder = new NewOrderInfo();
        newOrder.setItemId(itemId);
        newOrder.setItemTitle(item.getName());
        newOrder.setDescription(item.getDescription());
        newOrder.setPrice(item.getPrice());
        return newOrder;
    }

    @Override
    public void changeStatus(OrderIdStatusDTO orderDTO) {
        Order order = orderDao.findOne(new OrderId(orderDTO.getUserId(), orderDTO.getItemId()));
        order.setStatus(orderDTO.getStatus());
        orderDao.update(order);
    }
}
