package com.gmail.constantin.spirin.book.store.app.service.converter.entity;

import org.springframework.stereotype.Component;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Profile;
import com.gmail.constantin.spirin.book.store.app.service.dto.ProfileDTO;

import java.util.List;
import java.util.stream.Collectors;

@Component("profileConverter")
public class ProfileConverter implements Converter<ProfileDTO, Profile> {
    @Override
    public Profile toEntity(ProfileDTO dto) {
        Profile profile = new Profile();
        profile.setAddress(dto.getAddress());
        profile.setTelephone(dto.getTelephone());
        return profile;
    }

    @Override
    public List<Profile> toEntityList(List<ProfileDTO> list) {
        return list.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
