package com.gmail.constantin.spirin.book.store.app.service.dto;

import org.springframework.web.multipart.MultipartFile;

public class MultipartFileDTO {

    MultipartFile multipartFile;

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
