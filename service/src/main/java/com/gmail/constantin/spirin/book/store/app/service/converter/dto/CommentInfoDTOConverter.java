package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Comment;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import com.gmail.constantin.spirin.book.store.app.service.dto.CommentInfo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("commentInfoDTOConverter")
public class CommentInfoDTOConverter implements DTOConverter<CommentInfo, Comment> {

    @Override
    public CommentInfo toDTO(Comment entity) {
        CommentInfo commentInfo = new CommentInfo();
        User author = entity.getUser();
        commentInfo.setAuthor(author.getName() + " " + author.getSurname());
        commentInfo.setContent(entity.getContent());
        commentInfo.setCreated(entity.getCreated());
        commentInfo.setId(entity.getId());
        commentInfo.setUserId(author.getId());
        return commentInfo;
    }

    @Override
    public List<CommentInfo> toDTOList(List<Comment> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
