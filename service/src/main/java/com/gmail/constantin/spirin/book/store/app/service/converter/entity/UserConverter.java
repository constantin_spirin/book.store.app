package com.gmail.constantin.spirin.book.store.app.service.converter.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.gmail.constantin.spirin.book.store.app.dao.RoleDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Profile;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Role;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserDTO;
import com.gmail.constantin.spirin.book.store.app.service.enums.RoleType;

import java.util.List;
import java.util.stream.Collectors;

@Component("userConverter")
public class UserConverter implements Converter<UserDTO, User> {

    private final ProfileConverter profileConverter;
    private final RoleDao roleDao;

    @Autowired
    public UserConverter(
            @Qualifier("profileConverter") ProfileConverter profileConverter,
            @Qualifier("roleDao") RoleDao roleDao
    ) {
        this.profileConverter = profileConverter;
        this.roleDao = roleDao;
    }

    @Override
    public User toEntity(UserDTO dto) {
        User user = new User();
        user.setId(dto.getId());
        user.setEmail(dto.getEmail());
        user.setName(dto.getName());
        user.setSurname(dto.getSurname());
        user.setPassword(dto.getPassword());
        Profile profile = profileConverter.toEntity(dto.getProfileDTO());
        profile.setUser(user);
        user.setProfile(profile);
        Role role;
        if (dto.getRoleId() == null) {
            role = roleDao.getRoleByName(RoleType.CUSTOMER.toString());
            user.setRole(role);
        } else {
            role = roleDao.findOne(dto.getRoleId());
        }
        user.setRole(role);
        return user;
    }

    @Override
    public List<User> toEntityList(List<UserDTO> list) {
        return list.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
