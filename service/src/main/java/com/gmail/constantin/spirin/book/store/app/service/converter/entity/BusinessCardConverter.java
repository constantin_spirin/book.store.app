package com.gmail.constantin.spirin.book.store.app.service.converter.entity;

import org.springframework.stereotype.Component;
import com.gmail.constantin.spirin.book.store.app.dao.entity.BusinessCard;
import com.gmail.constantin.spirin.book.store.app.service.dto.BusinessCardDTO;

import java.util.List;
import java.util.stream.Collectors;

@Component("businessCardConverter")
public class BusinessCardConverter implements Converter<BusinessCardDTO, BusinessCard> {
    @Override
    public BusinessCard toEntity(BusinessCardDTO dto) {
        BusinessCard businessCard = new BusinessCard();
        businessCard.setId(dto.getId());
        businessCard.setTitle(dto.getTitle());
        businessCard.setFullName(dto.getFullName());
        businessCard.setWorkingTelephone(dto.getWorkingTelephone());
        return businessCard;
    }

    @Override
    public List<BusinessCard> toEntityList(List<BusinessCardDTO> list) {
        return list.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
