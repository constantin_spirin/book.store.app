package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import com.gmail.constantin.spirin.book.store.app.dao.entity.BusinessCard;
import com.gmail.constantin.spirin.book.store.app.service.dto.BusinessCardDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("businessCardDTOConverter")
public class BusinessCardDTOConverter implements DTOConverter<BusinessCardDTO, BusinessCard> {
    @Override
    public BusinessCardDTO toDTO(BusinessCard entity) {
        BusinessCardDTO businessCardDTO = new BusinessCardDTO();
        businessCardDTO.setId(entity.getId());
        businessCardDTO.setTitle(entity.getTitle());
        businessCardDTO.setFullName(entity.getFullName());
        businessCardDTO.setWorkingTelephone(entity.getWorkingTelephone());
        return businessCardDTO;
    }

    @Override
    public List<BusinessCardDTO> toDTOList(List<BusinessCard> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
