package com.gmail.constantin.spirin.book.store.app.service.impl;

import com.gmail.constantin.spirin.book.store.app.dao.ItemDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Item;
import com.gmail.constantin.spirin.book.store.app.service.converter.dto.ItemDTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.entity.ItemConverter;
import com.gmail.constantin.spirin.book.store.app.service.util.pagination.Pagination;
import com.gmail.constantin.spirin.book.store.app.service.util.pagination.properties.PaginationProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.gmail.constantin.spirin.book.store.app.service.ItemService;
import com.gmail.constantin.spirin.book.store.app.service.dto.ItemDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.MultipartFileDTO;
import com.gmail.constantin.spirin.book.store.app.service.util.fromXmlConvert.ItemsFromXMLConverter;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

    private final ItemDTOConverter itemDTOConverter;
    private final ItemConverter itemConverter;
    private final ItemsFromXMLConverter itemsFromXMLConverter;
    private final PaginationProperties paginationProperties;
    private final ItemDao itemDao;

    @Autowired
    public ItemServiceImpl(
            @Qualifier("itemDTOConverter") ItemDTOConverter itemDTOConverter,
            @Qualifier("itemConverter") ItemConverter itemConverter,
            ItemsFromXMLConverter itemsFromXMLConverter,
            PaginationProperties paginationProperties,
            ItemDao itemDao
    ) {
        this.itemDTOConverter = itemDTOConverter;
        this.itemConverter = itemConverter;
        this.itemsFromXMLConverter = itemsFromXMLConverter;
        this.paginationProperties = paginationProperties;
        this.itemDao = itemDao;
    }

    @Override
    public ItemDTO save(ItemDTO dto) {
        Item item = itemConverter.toEntity(dto);
        itemDao.create(item);
        return itemDTOConverter.toDTO(item);
    }

    @Override
    public List<ItemDTO> findAll(int page) {
        int limit = paginationProperties.getItemsCountOnPage();
        int first = Pagination.getFirst(page, limit);
        return itemDTOConverter.toDTOList(itemDao.findAll(first, limit));
    }

    @Override
    public int getPagesCount() {
        int rowsCount = itemDao.getRowsCount();
        return Pagination.pagesCount(rowsCount, paginationProperties.getItemsCountOnPage());
    }

    @Override
    public void delete(Long id) {
        Item item = itemDao.findOne(id);
        item.setDeleted(true);
    }

    @Override
    public void update(ItemDTO itemDTO, Long id) {
        itemDTO.setId(id);
        Item item = itemConverter.toEntity(itemDTO);
        itemDao.update(item);
    }

    @Override
    public ItemDTO findById(Long id) {
        Item item = itemDao.findOne(id);
        return itemDTOConverter.toDTO(item);
    }

    @Override
    public List<ItemDTO> saveItemsFromXml(MultipartFileDTO multipartFileDTO) {
        List<ItemDTO> items = new ArrayList<>();
        MultipartFile multipartFile = multipartFileDTO.getMultipartFile();
        try {
            items = itemsFromXMLConverter.toDTOList(multipartFile);
            for (ItemDTO item : items) {
                save(item);
            }
        } catch (Exception e) {
            logger.error("Failed to save items from xml file", e);
        }
        return items;
    }
}
