package com.gmail.constantin.spirin.book.store.app.service.enums;

public enum StatusType {
    NEW, REVIEWING, IN_PROGRESS, DELIVERED;
    StatusType() {
    }
}
