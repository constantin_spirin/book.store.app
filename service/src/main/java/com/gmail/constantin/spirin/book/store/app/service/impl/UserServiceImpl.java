package com.gmail.constantin.spirin.book.store.app.service.impl;

import com.gmail.constantin.spirin.book.store.app.dao.UserDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Role;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import com.gmail.constantin.spirin.book.store.app.service.converter.dto.DTOConverter;
import com.gmail.constantin.spirin.book.store.app.service.converter.entity.Converter;
import com.gmail.constantin.spirin.book.store.app.service.converter.entity.RoleConverter;
import com.gmail.constantin.spirin.book.store.app.service.dto.RoleDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.UserInfo;
import com.gmail.constantin.spirin.book.store.app.service.util.ServiceUtil;
import com.gmail.constantin.spirin.book.store.app.service.util.pagination.Pagination;
import com.gmail.constantin.spirin.book.store.app.service.util.pagination.properties.PaginationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import com.gmail.constantin.spirin.book.store.app.service.UserService;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final DTOConverter<UserDTO, User> userDTOConverter;
    private final Converter<UserDTO, User> userConverter;
    private final DTOConverter<UserInfo, User> userInfoDTOConverter;
    private final RoleConverter roleConverter;
    private final ServiceUtil serviceUtil;
    private final PaginationProperties paginationProperties;

    @Autowired
    public UserServiceImpl(
            @Qualifier("userDao") UserDao userDao,
            @Qualifier("userDTOConverter") DTOConverter<UserDTO, User> userDTOConverter,
            @Qualifier("userConverter") Converter<UserDTO, User> userConverter,
            @Qualifier("userInfoDTOConverter") DTOConverter<UserInfo, User> userInfoDTOConverter,
            @Qualifier("roleConverter") RoleConverter roleConverter,
            ServiceUtil serviceUtil,
            PaginationProperties paginationProperties
    ) {
        this.userDao = userDao;
        this.userDTOConverter = userDTOConverter;
        this.userConverter = userConverter;
        this.userInfoDTOConverter = userInfoDTOConverter;
        this.roleConverter = roleConverter;
        this.serviceUtil = serviceUtil;
        this.paginationProperties = paginationProperties;
    }

    @Override
    public UserDTO save(UserDTO userDTO) {
        User user = userConverter.toEntity(userDTO);
        user.setPassword(BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt()));
        userDao.create(user);
        return userDTOConverter.toDTO(user);
    }

    @Override
    public List<UserInfo> findUsersInfo(int page) {
        int limit = paginationProperties.getUsersCountOnPage();
        int first = Pagination.getFirst(page, limit);
        return userInfoDTOConverter.toDTOList(userDao.findAll(first, limit));
    }

    @Override
    public UserDTO findById(Long id) {
        User user = userDao.findOne(id);
        return userDTOConverter.toDTO(user);
    }

    @Override
    public UserDTO findUserByEmail(String email) {
        User user = userDao.findUserByEmail(email);
        if (user != null) {
            return userDTOConverter.toDTO(user);
        } else {
            return null;
        }
    }

    @Override
    public UserDTO getCurrentUserDTO() {
        User currentUser = serviceUtil.getCurrentUser();
        currentUser.getProfile();
        return userDTOConverter.toDTO(currentUser);
    }

    @Override
    public List<UserDTO> findAll() {
        return userDTOConverter.toDTOList(userDao.findAll());
    }

    @Override
    public void updatePassword(UserDTO userDTO) {
        User user = userDao.findOne(userDTO.getId());
        user.setPassword(BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt()));
    }

    @Override
    public void updateRole(UserDTO userDTO) {
        User user = userDao.findOne(userDTO.getId());
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(userDTO.getRoleId());
        Role role = roleConverter.toEntity(roleDTO);
        user.setRole(role);
    }

    @Override
    public void disableUser(Long id) {
        User user = userDao.findOne(id);
        user.setEnabled(false);
    }

    @Override
    public void enableUser(Long id) {
        User user = userDao.findOne(id);
        user.setEnabled(true);
    }

    @Override
    public void deleteUser(Long id) {
        User user = userDao.findOne(id);
        user.setDeleted(true);
    }

    @Override
    public void restoreUser(Long id) {
        User user = userDao.findOne(id);
        user.setDeleted(false);
    }
}