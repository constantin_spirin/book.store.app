package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Profile;
import com.gmail.constantin.spirin.book.store.app.service.dto.ProfileDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("profileDTOConverter")
public class ProfileDTOConverter implements DTOConverter<ProfileDTO, Profile> {
    @Override
    public ProfileDTO toDTO(Profile entity) {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setAddress(entity.getAddress());
        profileDTO.setTelephone(entity.getTelephone());
        return profileDTO;
    }

    @Override
    public List<ProfileDTO> toDTOList(List<Profile> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
