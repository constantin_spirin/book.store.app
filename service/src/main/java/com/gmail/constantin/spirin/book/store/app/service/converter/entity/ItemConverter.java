package com.gmail.constantin.spirin.book.store.app.service.converter.entity;

import org.springframework.stereotype.Component;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Item;
import com.gmail.constantin.spirin.book.store.app.service.dto.ItemDTO;

import java.util.List;
import java.util.stream.Collectors;

@Component("itemConverter")
public class ItemConverter implements Converter<ItemDTO, Item> {
    @Override
    public Item toEntity(ItemDTO dto) {
        Item item = new Item();
        item.setId(dto.getId());
        item.setName(dto.getName());
        item.setDescription(dto.getDescription());
        item.setPrice(dto.getPrice());
        item.setUniqueNumber(dto.getUniqueNumber());
        return item;
    }

    @Override
    public List<Item> toEntityList(List<ItemDTO> list) {
        return list.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
