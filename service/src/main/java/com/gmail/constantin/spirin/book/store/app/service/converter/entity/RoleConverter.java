package com.gmail.constantin.spirin.book.store.app.service.converter.entity;

import org.springframework.stereotype.Component;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Role;
import com.gmail.constantin.spirin.book.store.app.service.dto.RoleDTO;

import java.util.List;
import java.util.stream.Collectors;

@Component("roleConverter")
public class RoleConverter implements Converter<RoleDTO, Role> {
    @Override
    public Role toEntity(RoleDTO dto) {
        Role role = new Role();
        role.setId(dto.getId());
        role.setName(dto.getName());
        return role;
    }

    @Override
    public List<Role> toEntityList(List<RoleDTO> list) {
        return list.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
