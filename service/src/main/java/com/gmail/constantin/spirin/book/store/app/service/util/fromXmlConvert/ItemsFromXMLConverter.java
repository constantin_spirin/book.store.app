package com.gmail.constantin.spirin.book.store.app.service.util.fromXmlConvert;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import com.gmail.constantin.spirin.book.store.app.service.dto.ItemDTO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ItemsFromXMLConverter {

    public List<ItemDTO> toDTOList(MultipartFile xmlFile) throws JAXBException, IOException {
        List<ItemDTO> itemDTOs = new ArrayList<>();

        JAXBContext jaxbContext = JAXBContext.newInstance(ItemsElement.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        ItemsElement items = (ItemsElement) jaxbUnmarshaller.unmarshal(xmlFile.getInputStream());

        for (ItemElement item : items.getItems()) {
            ItemDTO itemDTO = new ItemDTO();
            itemDTO.setName(item.getTitle());
            itemDTO.setDescription(item.getDescription());
            itemDTO.setUniqueNumber(item.getUniqueNumber());
            itemDTO.setPrice(item.getPrice());
            itemDTOs.add(itemDTO);
        }
        return itemDTOs;
    }
}
