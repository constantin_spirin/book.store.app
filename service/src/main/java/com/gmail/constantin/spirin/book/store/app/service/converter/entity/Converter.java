package com.gmail.constantin.spirin.book.store.app.service.converter.entity;

import java.util.List;

public interface Converter<D, E> {

    E toEntity(D dto);

    List<E> toEntityList(List<D> list);
}
