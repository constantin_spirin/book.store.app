package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import com.gmail.constantin.spirin.book.store.app.dao.entity.News;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewsInfo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("newsInfoDTOConverter")
public class NewsInfoDTOConverter implements DTOConverter<NewsInfo, News> {
    @Override
    public NewsInfo toDTO(News entity) {
        NewsInfo newsInfo = new NewsInfo();
        newsInfo.setId(entity.getId());
        newsInfo.setCreated(entity.getCreated());
        newsInfo.setTitle(entity.getTitle());
        newsInfo.setUserId(entity.getUser().getId());
        newsInfo.setAuthor(entity.getUser().getName() + " " + entity.getUser().getSurname());
        return newsInfo;
    }

    @Override
    public List<NewsInfo> toDTOList(List<News> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
