package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import com.gmail.constantin.spirin.book.store.app.dao.entity.News;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewsDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("newsDTOConverter")
public class NewsDTOConverter implements DTOConverter<NewsDTO, News> {
    @Override
    public NewsDTO toDTO(News entity) {
        NewsDTO newsDTO = new NewsDTO();
        newsDTO.setTitle(entity.getTitle());
        newsDTO.setContent(entity.getContent());
        newsDTO.setId(entity.getId());
        newsDTO.setCreated(entity.getCreated());
        return newsDTO;
    }

    @Override
    public List<NewsDTO> toDTOList(List<News> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
