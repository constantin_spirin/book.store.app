package com.gmail.constantin.spirin.book.store.app.service.dto;

public class OrderForSellerDTO {

    private OrderForCustomerDTO orderForCustomerDTO;
    private Long userId;
    private Long itemId;

    public OrderForCustomerDTO getOrderForCustomerDTO() {
        return orderForCustomerDTO;
    }

    public void setOrderForCustomerDTO(OrderForCustomerDTO orderForCustomerDTO) {
        this.orderForCustomerDTO = orderForCustomerDTO;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }
}
