package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Order;
import com.gmail.constantin.spirin.book.store.app.service.dto.OrderForSellerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("orderForSellerDTOConverter")
public class OrderForSellerDTOConverter implements DTOConverter<OrderForSellerDTO, Order> {

    private final OrderForCustomerDTOConverter orderForCustomerDTOConverter;

    @Autowired
    public OrderForSellerDTOConverter(
            @Qualifier("orderForCustomerDTOConverter") OrderForCustomerDTOConverter orderForCustomerDTOConverter
    ) {
        this.orderForCustomerDTOConverter = orderForCustomerDTOConverter;
    }

    @Override
    public OrderForSellerDTO toDTO(Order entity) {
        OrderForSellerDTO orderForSeller = new OrderForSellerDTO();
        orderForSeller.setItemId(entity.getItem().getId());
        orderForSeller.setUserId(entity.getUser().getId());
        orderForSeller.setOrderForCustomerDTO(orderForCustomerDTOConverter.toDTO(entity));
        return orderForSeller;
    }

    @Override
    public List<OrderForSellerDTO> toDTOList(List<Order> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
