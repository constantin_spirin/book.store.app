package com.gmail.constantin.spirin.book.store.app.service;

import com.gmail.constantin.spirin.book.store.app.service.dto.NewsDTO;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewsInfo;

import java.util.List;

public interface NewsService {
    NewsDTO save(NewsDTO dto);

    List<NewsInfo> findAll(int page);

    NewsDTO findById(Long id);

    void update(NewsDTO newsDTO, Long id);

    void delete(Long id);

    int getPagesCount();
}
