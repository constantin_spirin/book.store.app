package com.gmail.constantin.spirin.book.store.app.service.converter.dto;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Item;
import com.gmail.constantin.spirin.book.store.app.service.dto.ItemDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("itemDTOConverter")
public class ItemDTOConverter implements DTOConverter<ItemDTO, Item> {
    @Override
    public ItemDTO toDTO(Item entity) {
        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(entity.getId());
        itemDTO.setName(entity.getName());
        itemDTO.setDescription(entity.getDescription());
        itemDTO.setPrice(entity.getPrice());
        itemDTO.setUniqueNumber(entity.getUniqueNumber());
        return itemDTO;
    }

    @Override
    public List<ItemDTO> toDTOList(List<Item> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
