package com.gmail.constantin.spirin.book.store.app.service.converter.entity;

import com.gmail.constantin.spirin.book.store.app.dao.entity.News;
import com.gmail.constantin.spirin.book.store.app.service.dto.NewsDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("newsConverter")
public class NewsConverter implements Converter<NewsDTO, News> {
    @Override
    public News toEntity(NewsDTO dto) {
        News news = new News();
        news.setId(dto.getId());
        news.setTitle(dto.getTitle());
        news.setContent(dto.getContent());
        news.setCreated(dto.getCreated());
        return news;
    }

    @Override
    public List<News> toEntityList(List<NewsDTO> list) {
        return list.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
