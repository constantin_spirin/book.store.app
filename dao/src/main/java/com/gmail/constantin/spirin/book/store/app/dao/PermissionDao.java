package com.gmail.constantin.spirin.book.store.app.dao;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Permission;

public interface PermissionDao extends GenericDao<Permission> {
}
