package com.gmail.constantin.spirin.book.store.app.dao.impl;

import com.gmail.constantin.spirin.book.store.app.dao.UserDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userDao")
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

    public UserDaoImpl() {
        super(User.class);
    }

    @Override
    public User findUserByEmail(String email) {
        String hql = "FROM User u WHERE u.email=:email AND u.isDeleted=false";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("email", email);
        return (User) query.uniqueResult();
    }

    @Override
    public List<User> findAll(int first, int limit) {
        Query query = getCurrentSession().createQuery(
                "FROM User u ORDER BY u.id"
        );
        query.setFirstResult(first);
        query.setMaxResults(limit);
        return query.list();
    }

    @Override
    public int getRowsCount() {
        Query query = getCurrentSession().createQuery(
                "select count(*) from User u"
        );
        return ((Long) query.uniqueResult()).intValue();
    }
}
