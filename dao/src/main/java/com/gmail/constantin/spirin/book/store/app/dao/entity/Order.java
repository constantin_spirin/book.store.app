package com.gmail.constantin.spirin.book.store.app.dao.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "T_ORDER")
public class Order implements Serializable {
    @EmbeddedId
    private OrderId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("USER_ID")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("ITEM_ID")
    private Item item;

    @Column(name = "CREATED")
    private LocalDateTime created;

    @Column(name = "QUANTITY")
    private Integer quantity;

    @Column(name = "STATUS")
    private String status;

    public OrderId getId() {
        return id;
    }

    public void setId(OrderId id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return Objects.equals(getId(), order.getId()) &&
                Objects.equals(getUser(), order.getUser()) &&
                Objects.equals(getItem(), order.getItem()) &&
                Objects.equals(getCreated(), order.getCreated()) &&
                Objects.equals(getQuantity(), order.getQuantity()) &&
                getStatus() == order.getStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser(), getItem(), getCreated(), getQuantity(), getStatus());
    }
}
