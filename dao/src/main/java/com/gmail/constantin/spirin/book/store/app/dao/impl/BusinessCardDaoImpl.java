package com.gmail.constantin.spirin.book.store.app.dao.impl;

import com.gmail.constantin.spirin.book.store.app.dao.BusinessCardDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.BusinessCard;
import org.springframework.stereotype.Repository;

@Repository("businessCardDao")
public class BusinessCardDaoImpl extends GenericDaoImpl<BusinessCard> implements BusinessCardDao {

    public BusinessCardDaoImpl() {
        super(BusinessCard.class);
    }
}