package com.gmail.constantin.spirin.book.store.app.dao.impl;

import com.gmail.constantin.spirin.book.store.app.dao.ItemDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Item;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository("itemDao")
public class ItemDaoImpl extends GenericDaoImpl<Item> implements ItemDao {
    public ItemDaoImpl() {
        super(Item.class);
    }

    @Override
    public Integer getCountItemsInPriceDiapason(BigDecimal minPrice, BigDecimal maxPrice) {
        String hql = "SELECT COUNT(*) FROM Item i WHERE i.price>=:minPrice AND i.price<=:maxPrice";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("minPrice", minPrice);
        query.setParameter("maxPrice", maxPrice);
        return ((Long) query.uniqueResult()).intValue();
    }
}