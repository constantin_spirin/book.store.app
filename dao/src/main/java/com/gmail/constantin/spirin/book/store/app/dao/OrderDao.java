package com.gmail.constantin.spirin.book.store.app.dao;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Order;
import com.gmail.constantin.spirin.book.store.app.dao.entity.OrderId;

public interface OrderDao extends GenericDao<Order> {

    Order findOne(OrderId orderId);
}