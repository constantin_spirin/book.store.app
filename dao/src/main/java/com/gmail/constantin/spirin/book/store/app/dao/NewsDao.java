package com.gmail.constantin.spirin.book.store.app.dao;

import com.gmail.constantin.spirin.book.store.app.dao.entity.News;

public interface NewsDao extends GenericDao<News> {
}
