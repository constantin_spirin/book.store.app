package com.gmail.constantin.spirin.book.store.app.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface GenericDao<T extends Serializable> {
    T findOne(final long entityId);

    List<T> findAll();

    List<T> findAll(int first, int limit);

    int getRowsCount();

    void create(final T entity);

    void update(final T entity);

    void delete(final T entity);

    void deleteById(final long entityId);

    Session getCurrentSession();
}
