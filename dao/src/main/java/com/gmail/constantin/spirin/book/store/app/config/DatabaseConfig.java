package com.gmail.constantin.spirin.book.store.app.config;

import com.gmail.constantin.spirin.book.store.app.config.properties.DatabaseProperties;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Audit;
import com.gmail.constantin.spirin.book.store.app.dao.entity.BusinessCard;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Comment;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Item;
import com.gmail.constantin.spirin.book.store.app.dao.entity.News;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Order;
import com.gmail.constantin.spirin.book.store.app.dao.entity.OrderId;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Permission;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Profile;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Role;
import com.gmail.constantin.spirin.book.store.app.dao.entity.User;
import com.zaxxer.hikari.HikariDataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class DatabaseConfig {

    private final DatabaseProperties databaseProperties;

    @Autowired
    public DatabaseConfig(DatabaseProperties databaseProperties) {
        this.databaseProperties = databaseProperties;
    }

    @Bean
    public DataSource dataSource() {
        final HikariDataSource ds = new HikariDataSource();
        ds.setPoolName("App connection pool");
        ds.setMaximumPoolSize(databaseProperties.getMaxPoolSize());
        ds.setDataSourceClassName(databaseProperties.getDataSourceClass());
        ds.addDataSourceProperty("url", databaseProperties.getUrl());
        ds.addDataSourceProperty("user", databaseProperties.getUsername());
        ds.addDataSourceProperty("password", databaseProperties.getPassword());
        ds.addDataSourceProperty("cachePrepStmts", databaseProperties.getCachePreparedStatements());
        ds.addDataSourceProperty("prepStmtCacheSize", databaseProperties.getCachePreparedStatementsSize());
        ds.addDataSourceProperty("prepStmtCacheSqlLimit", databaseProperties.getCachePreparedStatementsSQLLimit());
        ds.addDataSourceProperty("useServerPrepStmts", databaseProperties.getUseServerPreparedStatements());
        return ds;
    }

    @Bean
    public SpringLiquibase springLiquibase(DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();

        liquibase.setDataSource(dataSource);
        liquibase.setDropFirst(Boolean.TRUE);
        liquibase.setChangeLog("classpath:migration/db-changelog.xml");
        return liquibase;
    }

    @Bean
    @DependsOn("springLiquibase")
    public LocalSessionFactoryBean getSessionFactory(DataSource dataSource) {
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        Properties props = new Properties();
        props.put(Environment.DIALECT, databaseProperties.getDialect());
        props.put(Environment.SHOW_SQL, databaseProperties.getShowSQL());
        props.put(Environment.HBM2DDL_AUTO, databaseProperties.getHBM2DDLAuto());
        props.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperties.getSecondLevelCache());
        props.put(Environment.CACHE_REGION_FACTORY, databaseProperties.getFactoryClass());
        factoryBean.setHibernateProperties(props);
        factoryBean.setAnnotatedClasses(
                User.class,
                Audit.class,
                News.class,
                Profile.class,
                Comment.class,
                Order.class,
                OrderId.class,
                Item.class,
                Role.class,
                Permission.class,
                BusinessCard.class
        );
        return factoryBean;
    }

    @Bean
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory);
        return transactionManager;
    }
}
