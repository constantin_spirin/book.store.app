package com.gmail.constantin.spirin.book.store.app.dao;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Profile;

public interface ProfileDao extends GenericDao<Profile> {
}
