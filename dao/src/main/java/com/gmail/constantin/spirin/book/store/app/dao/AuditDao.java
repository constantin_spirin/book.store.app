package com.gmail.constantin.spirin.book.store.app.dao;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Audit;

public interface AuditDao extends GenericDao<Audit> {

}
