package com.gmail.constantin.spirin.book.store.app.dao.impl;

import com.gmail.constantin.spirin.book.store.app.dao.CommentDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Comment;
import com.gmail.constantin.spirin.book.store.app.dao.entity.News;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("commentDao")
public class CommentDaoImpl extends GenericDaoImpl<Comment> implements CommentDao {
    public CommentDaoImpl() {
        super(Comment.class);
    }

    @Override
    public List<Comment> findCommentsByNews(int first, int limit, News news) {
        Query query = getCurrentSession().createQuery("FROM Comment c WHERE c.news=:news ORDER BY c.created DESC");
        query.setParameter("news", news);
        query.setFirstResult(first);
        query.setMaxResults(limit);
        return query.list();
    }
}
