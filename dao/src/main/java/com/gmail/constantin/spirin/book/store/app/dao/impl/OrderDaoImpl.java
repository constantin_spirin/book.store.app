package com.gmail.constantin.spirin.book.store.app.dao.impl;

import com.gmail.constantin.spirin.book.store.app.dao.OrderDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Order;
import com.gmail.constantin.spirin.book.store.app.dao.entity.OrderId;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

@Repository("orderDao")
public class OrderDaoImpl extends GenericDaoImpl<Order> implements OrderDao {
    public OrderDaoImpl() {
        super(Order.class);
    }

    @Override
    public Order findOne(OrderId orderId) {
        String hql = "FROM Order o WHERE o.id=:orderId";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("orderId", orderId);
        return (Order) query.uniqueResult();
    }
}
