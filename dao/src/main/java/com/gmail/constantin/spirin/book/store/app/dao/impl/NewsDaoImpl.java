package com.gmail.constantin.spirin.book.store.app.dao.impl;

import com.gmail.constantin.spirin.book.store.app.dao.NewsDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.News;
import org.springframework.stereotype.Repository;

@Repository("newsDao")
public class NewsDaoImpl extends GenericDaoImpl<News> implements NewsDao {
    public NewsDaoImpl() {
        super(News.class);
    }
}
