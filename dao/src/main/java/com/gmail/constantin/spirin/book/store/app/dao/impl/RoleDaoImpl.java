package com.gmail.constantin.spirin.book.store.app.dao.impl;

import com.gmail.constantin.spirin.book.store.app.dao.RoleDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Role;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

@Repository("roleDao")
public class RoleDaoImpl extends GenericDaoImpl<Role> implements RoleDao {
    public RoleDaoImpl() {
        super(Role.class);
    }

    @Override
    public Role getRoleByName(String name) {
        String hql = "FROM Role r WHERE r.name=:name";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("name", name);
        return (Role) query.uniqueResult();
    }
}
