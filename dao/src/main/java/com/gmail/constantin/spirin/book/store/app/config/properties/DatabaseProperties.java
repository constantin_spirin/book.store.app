package com.gmail.constantin.spirin.book.store.app.config.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DatabaseProperties {

    @Autowired
    private Environment environment;

    private String driverName;
    private String url;
    private String username;
    private String password;

    private String hbm2DDLAuto;
    private String currentSessionContextClass;
    private String dialect;
    private String showSQL;
    private String secondLevelCache;
    private String factoryClass;
    private String dataSourceClass;
    private Integer maxPoolSize;
    private String cachePreparedStatements;
    private String cachePreparedStatementsSize;
    private String cachePreparedStatementsSQLLimit;
    private String useServerPreparedStatements;

    @PostConstruct
    public void initialize() {
        this.driverName = environment.getProperty("database.driver.name");
        this.url = environment.getProperty("database.url");
        this.username = environment.getProperty("database.username");
        this.password = environment.getProperty("database.password");
        this.hbm2DDLAuto = environment.getProperty("hibernate.hbm2ddl.auto");
        this.currentSessionContextClass = environment.getProperty("hibernate.current_session_context_class");
        this.dialect = environment.getProperty("hibernate.dialect");
        this.showSQL = environment.getProperty("hibernate.show_sql");
        this.secondLevelCache = environment.getProperty("hibernate.cache.use_second_level_cache");
        this.factoryClass = environment.getProperty("hibernate.cache.region.factory_class");
        this.dataSourceClass = environment.getProperty("pool.data.source.class");
        this.maxPoolSize = Integer.valueOf(environment.getProperty("pool.max.size"));
        this.cachePreparedStatements = environment.getProperty("pool.cache.prepared.statements");
        this.cachePreparedStatementsSize = environment.getProperty("pool.cache.prepared.statement.size");
        this.cachePreparedStatementsSQLLimit = environment.getProperty("pool.cache.preparedstatements.sql.limit");
        this.useServerPreparedStatements = environment.getProperty("pool.use.server.prepared.statements");
    }

    public String getDriverName() {
        return driverName;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getHBM2DDLAuto() {
        return hbm2DDLAuto;
    }

    public String getCurrentSessionContextClass() {
        return currentSessionContextClass;
    }

    public String getDialect() {
        return dialect;
    }

    public String getShowSQL() {
        return showSQL;
    }

    public String getSecondLevelCache() {
        return secondLevelCache;
    }

    public String getFactoryClass() {
        return factoryClass;
    }

    public String getDataSourceClass() {
        return dataSourceClass;
    }

    public Integer getMaxPoolSize() {
        return maxPoolSize;
    }

    public String getCachePreparedStatements() {
        return cachePreparedStatements;
    }

    public String getCachePreparedStatementsSize() {
        return cachePreparedStatementsSize;
    }

    public String getCachePreparedStatementsSQLLimit() {
        return cachePreparedStatementsSQLLimit;
    }

    public String getUseServerPreparedStatements() {
        return useServerPreparedStatements;
    }
}
