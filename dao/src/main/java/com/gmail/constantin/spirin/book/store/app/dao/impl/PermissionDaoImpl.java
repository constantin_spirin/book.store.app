package com.gmail.constantin.spirin.book.store.app.dao.impl;

import com.gmail.constantin.spirin.book.store.app.dao.PermissionDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Permission;
import org.springframework.stereotype.Repository;

@Repository("permissionDao")
public class PermissionDaoImpl extends GenericDaoImpl<Permission> implements PermissionDao {
    public PermissionDaoImpl() {
        super(Permission.class);
    }
}
