package com.gmail.constantin.spirin.book.store.app.dao;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Role;

public interface RoleDao extends GenericDao<Role> {
    Role getRoleByName(String name);
}
