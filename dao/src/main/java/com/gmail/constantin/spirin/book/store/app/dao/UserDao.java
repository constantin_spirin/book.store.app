package com.gmail.constantin.spirin.book.store.app.dao;

import com.gmail.constantin.spirin.book.store.app.dao.entity.User;

public interface UserDao extends GenericDao<User> {

    User findUserByEmail(String email);

}
