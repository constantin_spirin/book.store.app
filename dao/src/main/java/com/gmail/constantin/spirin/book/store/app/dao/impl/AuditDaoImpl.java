package com.gmail.constantin.spirin.book.store.app.dao.impl;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Audit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import com.gmail.constantin.spirin.book.store.app.dao.AuditDao;

@Repository("auditDao")
public class AuditDaoImpl extends GenericDaoImpl<Audit> implements AuditDao {

    private static final Logger logger = LogManager.getLogger(AuditDaoImpl.class);

    public AuditDaoImpl() {
        super(Audit.class);
    }
}
