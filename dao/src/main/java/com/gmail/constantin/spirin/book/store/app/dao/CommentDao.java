package com.gmail.constantin.spirin.book.store.app.dao;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Comment;
import com.gmail.constantin.spirin.book.store.app.dao.entity.News;

import java.util.List;

public interface CommentDao extends GenericDao<Comment> {

    List<Comment> findCommentsByNews(int first, int limit, News news);
}
