package com.gmail.constantin.spirin.book.store.app.dao;

import com.gmail.constantin.spirin.book.store.app.dao.entity.Item;

import java.math.BigDecimal;

public interface ItemDao extends GenericDao<Item> {

    public Integer getCountItemsInPriceDiapason(BigDecimal minPrice, BigDecimal maxPrice);
}