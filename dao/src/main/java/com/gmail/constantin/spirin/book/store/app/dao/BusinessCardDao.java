package com.gmail.constantin.spirin.book.store.app.dao;

import com.gmail.constantin.spirin.book.store.app.dao.entity.BusinessCard;

public interface BusinessCardDao extends GenericDao<BusinessCard> {

}
