package com.gmail.constantin.spirin.book.store.app.dao.impl;

import com.gmail.constantin.spirin.book.store.app.dao.ProfileDao;
import com.gmail.constantin.spirin.book.store.app.dao.entity.Profile;
import org.springframework.stereotype.Repository;

@Repository("profileDao")
public class ProfileDaoImpl extends GenericDaoImpl<Profile> implements ProfileDao {
    public ProfileDaoImpl() {
        super(Profile.class);
    }
}
